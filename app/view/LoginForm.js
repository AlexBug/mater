/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

Ext.require('Ext.form.field.Checkbox');
Ext.define('mater.view.LoginForm', {
	extend: 'Ext.form.Panel',
	alias: 'widget.loginform',

    labelWidth: 70,
    welcomeMsg: new Ext.Template('<div class="welcome">Login</div>'),
    frame: true,
    plain: true,
    //layout: 'column',
    defaultType: 'textfield',
    bodyStyle: 'padding: 10px',
    items:[
        { xtype: 'box' },
        {
            fieldLabel: 'Username',
            name: 'username',
            // will be changed to false if the registered login
            // button is pushed
            allowBlank: true,
            maxLength: 17,
            maxLengthText: 'User names may not be longer than 17 characters.',
            minLength: 3,
            minLengthText: 'User names must be at least 3 characters.',
            msgTarget: 'title',
            tabIndex: 1,
            vtype: 'alpha'
        },{
            fieldLabel:'Password',
            name:'password',
            inputType:'password',
            allowBlank: false,
            tabIndex: 2,
        }
    ],


    buttons: [
        {
            text: 'Log in',
            formBind: true,
            tabIndex: 3,
            action: 'login',
            // Function that fires when user clicks the button
            /*handler: function() {
                log_in(login.form.findField('username').getValue(),
                    login.form.findField('password').getValue());
            }*/
        },
        {
            text: 'Enter as a guest',
            formBind: false,
            tabIndex: 4,
            action: 'guestlogin',
            /*handler: function() {
               log_in('guest', '')
             }*/
        }
    ],
    initComponent: function() {
        "use strict";
        this.items[0].html = this.welcomeMsg.apply({icsName: mater.icsName});
        this.superclass.initComponent.apply(this);
    }
});

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
