/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('mater.view.Console', {
    extend: 'Ext.window.Window',
    alias: 'widget.mainconsole',
    //width: '50%',
    //height: '40%',
    width: 650,
    height: 220,
    cls: 'consoleWin',
    icon: 'resources/img/console.png',
    closable: false,
    resizable: true,
    minimizable: true,
    plain: false,
    layout: 'vbox',
    border: true,
    x: 5,
    y: 0 + 40,
    bodyStyle: 'padding: 7px; background-color: #000000',
    items: [
        {
            xtype: 'container',
            id: 'consoleText',
            name: 'consoleText',
            autoScroll: true,
            flex: 1,
            width: '100%'
        },
        {
            /* ugly but I couldn't find another way to
             * force space here */
            xtype: 'container',
            width: '100%',
            height: 7
        },
        {
            id: 'commandText',
            xtype: 'textfield',
            width: '100%',
            tabIndex: 1
        }
    ],
    defaultFocus: 'commandText'
});

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
