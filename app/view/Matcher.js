/*
 * Copyright (C) 2014  Anthony Guo <anthony.guo@some.ox.ac.uk>
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('mater.view.Matcher', {
    extend: 'Ext.window.Window',
    requires: ['Ext.form.field.ComboBox','Ext.form.Label'],
    controller: 'matcher',
    alias: 'widget.matcher',
    width: 500,
    height: 240,
    autoShow: false,
    closable: true,
    resizable: false,
    minimizable: false,
    plain: false,
    border: true,
    title: 'Matcher',
    modal: true,
    closeAction: 'hide',
    outputError: function(errorMsg){
        "use strict";
        var errorMsgBox = Ext.ComponentQuery.query('label[name=errorLabel]')[0];
        errorMsgBox.setText(errorMsg);
    },
    getMatchDetails: function(){
        "use strict";
        var opName= Ext.ComponentQuery.query('textfield[name=opponentName]')[0].value;
        var initTime = Ext.ComponentQuery.query('textfield[name=initialTime]')[0].value;
        var inc = Ext.ComponentQuery.query('textfield[name=increment]')[0].value;
        var variant = Ext.ComponentQuery.query('combo[name=variant]')[0].value;
        return {
            'name' : opName,
            'initialTime': initTime,
            'increment': inc,
            'variant': variant
        };
    },
    items: [
        {
            xtype: 'container',
            itemId: 'matcherContainer',
            autoScroll: true,
            tpl: '{html}<br />',
            tplWriteMode: 'append',
            padding: '5px',
            height: 170,
            width: '100%',
            cls: 'newsText',
            layout: 'vbox',
            items: [
                {
                    xtype: 'textfield',
                    id: 'opponentName',
                    name: 'opponentName',
                    fieldLabel: 'Opponent',
                    labelAlign: 'left',
                    width: '95%',
                    allowBlank: false,
                    labelWidth: 130
                },
                {
                    xtype: 'textfield',
                    id: 'initialTime',
                    name: 'initialTime',
                    fieldLabel: 'Time (m)',
                    labelAlign: 'left',
                    width: '95%',
                    allowBlank: false,
                    labelWidth: 130
                },
                {
                    xtype: 'textfield',
                    id: 'increment',
                    name: 'increment',
                    fieldLabel: 'Increment (s)',
                    labelAlign: 'left',
                    width: '95%',
                    allowBlank: false,
                    labelWidth: 130
                },
                {
                    xtype: 'combo',
                    fieldLabel: 'Variant',
                    allowBlank: false,
                    renderTo: Ext.getBody(),
                    displayField: 'variant',
                    width: '95%',
                    editable: false,
                    labelWidth: 130,
                    store: Ext.getStore('GameVariants'),
                    queryMode: 'local',
                    mode: 'local',
                    name: 'variant',
                    id: 'variantComboBox'
                },
                {
                    xtype: 'button',
                    text: 'Send Match!',
                    id: 'sendMatch'
                },
                {
                    xtype: 'label',
                    name: 'errorLabel'
                }
            ]
        }
    ],


    initComponent: function() {
        "use strict";
        //READY FOR A HACK??
        this.items[0].items[3].store = Ext.getStore('GameVariants');
        //OH YEAH BRING IT ON
        
        this.superclass.initComponent.apply(this);
    }
});

/* vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
 */
