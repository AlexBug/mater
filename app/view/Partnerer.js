/*
 * Copyright (C) 2014  Anthony Guo <anthony.guo@some.ox.ac.uk>
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('mater.view.Partnerer', {
    extend: 'Ext.window.Window',
    requires: ['Ext.form.Label'],
    controller: 'matcher',
    alias: 'widget.partnerer',
    width: 500,
    height: 140,
    autoShow: false,
    closable: true,
    resizable: false,
    minimizable: false,
    plain: false,
    border: true,
    title: 'Matcher',
    modal: true,
    closeAction: 'hide',
    outputError: function(errorMsg){
        "use strict";
        var errorMsgBox = Ext.ComponentQuery.query('label[name=errorLabel]')[0];
        errorMsgBox.setText(errorMsg);
    },
    getRequestDetails: function(){
        "use strict";
        var partnerName = Ext.ComponentQuery.query('textfield[name=partnerName]')[0].value;
        return {
            'partnerName' : partnerName,
        };
    },
    items: [
        {
            xtype: 'container',
            itemId: 'matcherContainer',
            autoScroll: true,
            tpl: '{html}<br />',
            tplWriteMode: 'append',
            padding: '5px',
            height: 140,
            width: '100%',
            cls: 'newsText',
            layout: 'vbox',
            items: [
                {
                    xtype: 'textfield',
                    id: 'partnerName',
                    name: 'partnerName',
                    fieldLabel: 'Partner',
                    labelAlign: 'left',
                    width: '95%',
                    allowBlank: false,
                    labelWidth: 130
                },
                {
                    xtype: 'button',
                    text: 'Send Partnership Request',
                    id: 'sendPartnershipRequest'
                },
                {
                    xtype: 'label',
                    name: 'errorLabel'
                }
            ]
        }
    ],


    initComponent: function() {
        "use strict";
        this.superclass.initComponent.apply(this);
    }
});

/* vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
 */
