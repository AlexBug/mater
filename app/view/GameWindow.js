/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

/* global console:false */

Ext.define('mater.view.GameWindow', {
    extend: 'Ext.window.Window',
    models: ['Game'],
    alias: 'widget.gamewindow',
    autoShow: true,
    width: 650,
    height: 600,
    layout: 'border',
    x: 0, //Irrelevant. The position is set in the Game.js controller file
    y: 0, //Irrelevant. The position is set in the Game.js controller file
    closable: true,
    resizable: false,
    maximizable: true,
    plain: true,
    padding: 0,
    border: false,

    items: [{xtype: 'container', itemId: 'cboard', height: 445, width: 445,
        region: 'center'},
        {xtype: 'container', itemId: 'infoPane', cls: 'infoPane',
            region: 'east',
            layout: 'border', height: 445, width: 205,
            items: [{xtype: 'clock', itemId: 'topClock', labelText: '',
                    region: 'north'},
                {xtype: 'container', region: 'center',
                    itemId: 'centerPanel',
                    layout: 'vbox',
                    style: 'margin-top: 15px', items:
                    [{xtype: 'container', itemId: 'desc'},
                    {xtype: 'container', itemId: 'lastMove'},
                    {xtype: 'container', itemId: 'result'},
                    {xtype: 'toolbar', itemId: 'playingToolbar', hidden: true,
                        vertical: true,
                        items: [{xtype: 'button', itemId: 'drawButton',
                            text: 'Draw', cls: 'drawButton'},
                            {xtype: 'button', itemId: 'resignButton',
                                text: 'Resign'},
                            {text: 'Other...', menu:
                                [{itemId: 'abortButton', text: 'Abort'},
                                {itemId: 'adjournButton', text: 'Adjourn'}]}]},
                    {xtype: 'button', itemId: 'flipButton',
                        tooltip: 'Flip board', width: 25, height: 25,
                        cls: 'flipButton'},
                    {xtype: 'container', itemId: 'examiningButtons',
                        hidden: true, items:
                        [{xtype: 'button', itemId: 'back999Button',
                            tooltip: 'Back to beginning',
                            height: 24, width: 24,
                            cls: 'back999Button'},
                        {xtype: 'button', itemId: 'backButton',
                            tooltip: 'Back',
                            height: 24, width: 24,
                            cls: 'backButton'},
                        {xtype: 'button', itemId: 'forwardButton',
                            tooltip: 'Forward',
                            height: 24, width: 24,
                            cls: 'forwardButton'},
                        {xtype: 'button', itemId: 'forward999Button',
                            tooltip: 'Forward to end',
                            height: 24, width: 24,
                            cls: 'forward999Button'}]},
                    {xtype: 'button', itemId: 'closeButton', hidden: true,
                        text: 'Close', cls: 'closeButton'}]},
                {xtype: 'clock', itemId: 'bottomClock', labelText: '',
                    region: 'south'}]
        },
    ],

    initComponent: function() {
        "use strict";
        // fails in strict mode
        //this.callParent();
        if (this.game.amPlaying()) {
            if (this.game.black === mater.username) {
                this.icon = 'resources/img/bK.png';
            }
            else {
                console.assert(this.game.white === mater.username);
                this.icon = 'resources/img/wK.png';
            }
        }
        else if (this.game.isExamined()) {
            this.icon = 'resources/img/examine.png';
        }
        else if (this.game.isObserved()) {
            this.icon = 'resources/img/observe.png';
        }
        else {
            // should not happen
            console.assert(false);
        }
        /*this.on('resize', function(e, width, height, opts) {
            e.doComponentLayout();
            e.board.resize();
        });*/
        this.on('afterrender', function() {
            this.refreshInfo();
            this.down('#desc').update(this.desc);
            if (this.game.amPlaying()) {
                this.down('#centerPanel').down('#playingToolbar').show();
            }
            if (this.game.amExamining()) {
                this.down('#centerPanel').down('#examiningButtons').show();
            }
        });
        this.superclass.initComponent.apply(this);
        if (this.initialChat) {
            this.down('#chatTab').update(this.initialChat);
        }
    },

    gameOver: function(reason, result) {
        // called after this game has ended
        "use strict";
        this.down('#centerPanel').down('#playingToolbar').hide();
        this.down('#centerPanel').down('#closeButton').show();
        this.down('#topClock').down('#time').removeCls('clockToMove');
        this.down('#bottomClock').down('#time').removeCls('clockToMove');
        this.down('#result').update('{' + reason + '} ' + result);
        this.setTitle(this.title + ' - '+ result);
        this.refreshInfo();
    },

    refreshInfo: function() {
        // refresh info that changes only when the board is flipped
        // or the game ends; other info is refreshed in the Game controller
        "use strict";
        var whiteClock, blackClock;
        var w = this.white;
        var b = this.black;
        if (this.flipped) {
            whiteClock = this.down('#topClock');
            blackClock = this.down('#bottomClock');
        }
        else {
            whiteClock = this.down('#bottomClock');
            blackClock = this.down('#topClock');
        }
        if (this.whiteRating) {
            w += ' (' + this.whiteRating + ')';
            b += ' (' + this.blackRating + ')';
        }
        if (this.game.result === '1-0') {
            w =  '<span class="checkMark" style="color: green">&#x2713;</span> ' + w;
        }
        else if (this.game.result === '0-1') {
            b = '<span class="checkMark" style="color: green">&#x2713;</span> ' + b;
        }
        else if (this.game.result === '1/2-1/2') {
            w = '<span class="checkMark" style="color: #d9603b">=</span> ' + w;
            b ='<span class="checkMark" style="color: #d9603b">=</span> ' + b;
        }
        whiteClock.getComponent('label').update(w);
        blackClock.getComponent('label').update(b);
        whiteClock.down('#timeseal').setVisible(this.game.whiteHasTimeseal);
        blackClock.down('#timeseal').setVisible(this.game.blackHasTimeseal);
    },

    showLastMove: function(txt) {
        "use strict";
        this.down('#lastMove').update(txt);
    },

    flip: function() {
        "use strict";
        this.flipped = !this.flipped;
        this.board.orientation('flip');
        this.refreshInfo();
    }
});

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
