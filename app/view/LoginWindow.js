/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('mater.view.LoginWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.loginwindow',
    iconCls: 'tomatoIcon',
    autoShow: true,
    layout:'fit',
    width:300,
    height:190,
    closable: false,
    resizable: false,
    plain: true,
    border: false,
    items: { xtype: 'loginform'},
    initComponent: function() {
        "use strict";
        this.title = 'Mater';
        this.superclass.initComponent.apply(this);
    }
});

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
