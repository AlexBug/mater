/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

// hack to load non-standard statusbar class
Ext.Loader.setPath('Ext.ux', 'ext-4/examples/ux');
Ext.require('Ext.ux.statusbar.StatusBar');

Ext.define('mater.view.Chat', {
    extend: 'Ext.window.Window',
    requires: ['Ext.tab.Panel'],
    alias: 'widget.chat',
    width: 650,
    height: 220,
    autoShow: false,
    closable: false,
    resizable: true,
    minimizable: true,
    plain: false,
    layout: 'fit',
    border: true,
    bodyStyle: 'background-color: #ffffff',
    icon: 'resources/img/24px-speech_bubble.png',
    title: 'Chat',
    id: 'chatWindow',
    x: 660,
    y: 0 + 40,
    items: [{
        xtype: 'tabpanel',
        id: 'chatTabs',
        closable: false
    }],
    fbar: {
        xtype: 'textfield',
        id: 'chatInput',
        tabIndex: 2,
        width: '100%'
    },
    bbar: {xtype: 'statusbar', id: 'chatStatus'},
    defaultFocus: 'chatInput'
});

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
