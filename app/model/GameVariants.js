Ext.define('mater.model.GameVariants', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'int', type: 'int'},
        {name: 'variant', type: 'string'}
    ]
});
