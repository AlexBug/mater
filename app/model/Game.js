/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

/* global Chess, window, console */

Ext.define('mater.model.Game', {
    requires: ['mater.view.GameWindow'],
    extend: 'Ext.data.Model',
    fields: [
        {name: 'gameID', type: 'int', convert: null},
        {name: 'white', type: 'string'},
        {name: 'whiteRating', type: 'string'},
        {name: 'black', type: 'string'},
        {name: 'blackRating', type: 'string'},
        {name: 'desc', type: 'string'},
        {name: 'result', type: 'string'},
        {name: 'blackHand', type: 'string'}, // pieces in hand.
        {name: 'whiteHand', type: 'string'}  // pieces in hand.
        //{name: 'variant', type: 'string'},
        //{name: 'time', type: 'int', convert: null},
        //{name: 'inc', type: 'int', convert: null},
    ],

    constructor: function(config) {
        "use strict";
        this.active = true;
        this.white = config.white;
        this.whiteRating = config.whiteRating;
        this.black = config.black;
        this.blackRating = config.blackRating;
        this.result = '*';
        this.desc = config.desc;
        this.relation = config.relation;
        this.gameID = config.gameID;
        this.localFlip = false;
        this.clockIsTicking = false;
        this.whiteHasTimeseal = config.whiteHasTimeseal;
        this.blackHasTimeseal = config.blackHasTimeseal;
        this.chess = new Chess();
        // we haven't received the first move (ply for new games) yet
        this.ply = -2;
        this.lag = [0, 0];
        this.clockTimerId = null;
        this.blackHand = '';
        this.whiteHand = '';
        this.win = Ext.create('mater.view.GameWindow', {
            white: this.white,
            black: this.black,
            // silly jshint won't allow !!config.flip
            flipped: config.flip ? true : false,
            whiteRating: this.whiteRating,
            blackRating: this.blackRating,
            title: this.white + ' vs. ' + this.black,
            desc: this.desc,
            initialChat: config.initialChat,
            game: this
        });
        if (this.gameID in mater.model.Game) {
            console.log('overwriting old game ID ' + this.gameID);
        }
        mater.model.Game.byID[this.gameID] = this;
    },

    //verboseRe: /[PNBRQK]\/([a-h][1-8])-([a-h][1-8])(=[BNRQ])?/
    /*gotMove: function(mv) {
        var ret = this.game.move(mv);
        if (!ret) {
            alert('failed to execute move: ' + mv);
        }
        return ret;
    },*/
    gameEnd: function(reason, result) {
        // record a result for the game
        "use strict";
        this.resultReason = reason;
        this.result = result;
        this.win.gameOver(reason, result);
        this.stop();
    },

    stop: function() {
        // mark this game as no longer active
        "use strict";
        //console.log('stopping timer ' + this.clockTimerId);
        this.active = false;
        this.clockIsTicking = false;
        if (this.clockTimerId !== null) {
            window.clearTimeout(this.clockTimerId);
            this.clockTimerId = null;
        }
    },

    close: function() {
        // close the game window and remove references to this game
        "use strict";
        this.stop();
        if (this.win !== null) {
            this.win.close();
            this.win = null;
        }
        if (this.gameID !== null) {
            delete mater.model.Game.byID[this.gameID];
            this.gameID = null;
        }
    },

    isObserved: function() {
        "use strict";
        return this.relation === mater.model.Game.OBSERVING_EXAMINED || this.relation === mater.model.Game.OBSERVING_PLAYED;
    },

    amExamining: function() {
        "use strict";
        return this.relation === mater.model.Game.EXAMINING;
    },

    isExamined: function() {
        "use strict";
        return this.relation === mater.model.Game.OBSERVING_EXAMINED || this.relation === mater.model.Game.EXAMINING;
    },

    amPlaying: function() {
        "use strict";
        return (this.relation === mater.model.Game.PLAYING_OPPMOVE) || (this.relation === mater.model.Game.PLAYING_MYMOVE);
    },

    startClock: function() {
        "use strict";
        this.clockStartTime = new Date().getTime();
    },

    getWhiteTime: function() {
        "use strict";
        if (this.clockIsTicking && this.ply % 2 === 0) {
            return this.whiteTime - (new Date().getTime() - this.clockStartTime);
        }
        else {
            return this.whiteTime;
        }
    },

    getBlackTime: function() {
        "use strict";
        if (this.clockIsTicking && this.ply % 2 === 1) {
            return this.blackTime - (new Date().getTime() - this.clockStartTime);
        }
        else {
            return this.blackTime;
        }
    },

    // Checks whether <color> is holding <piece>
    handHas: function(color, piece) {
        "use strict";
        var normalized = this.normalizeNotation(piece);
        if (color === 'black' || color === 'b') {
            return this.blackHand.indexOf(normalized) >= 0;
        } else if (color === 'white' || color === 'w') {
            return this.whiteHand.indexOf(normalized) >= 0;
        }
    },

    // Convert notation to normal.
    // E.g.
    // bP => p
    // wQ => Q
    normalizeNotation: function(notation) {
        "use strict";
        if (notation.length === 2) {
            var color = notation.charAt(0),
                piece = notation.charAt(1);
            if (color === 'w') {
                return piece.toUpperCase();
            } else if (color === 'b') {
                return piece.toLowerCase();
            }
        }
    },

    // Returns the number of pieces that <color> is holding <piece>
    handNum: function(color, piece) {
        "use strict";
        var normalized = this.normalizeNotation(piece);
        if (color === 'black' || color === 'b') {
            return this.blackHand.split(normalized).length - 1;
        } else if (color === 'white' || color === 'w') {
            return this.whiteHand.split(normalized).length - 1;
        }
    },

    addChat: function(line) {
        "use strict";
        var tab = this.win.down('#chatTab');
        tab.update({line: line});
        tab.body.dom.scrollTop = 999999;
    },

    statics: {
        // used to look up games by ID
        byID: {},

        // relation constants, from style12
        INACTIVE: -4, // not used by FICS
        ISOLATED: -3,
        OBSERVING_EXAMINED: -2,
        EXAMINING: 2,
        PLAYING_OPPMOVE: -1,
        PLAYING_MYMOVE: 1,
        OBSERVING_PLAYED: 0
    }
});

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
