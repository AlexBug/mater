/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

/* global Ext, ChessBoard, console, window, mater, $:false */

function tryUntilSuccess(callback){
    "use strict";
    var success = callback();
    if (!success){
        setTimeout(function(){
            tryUntilSuccess(callback);
        }, 500);
    }
}


Ext.require('Ext.layout.container.Border');
Ext.define('mater.controller.Game', {
    extend: 'Ext.app.Controller',

    refs: [
        {ref: 'gamewindow', selector: 'gamewindow'},
    ],
    views: ['GameWindow', 'Clock'],
    models: ['Game'],

    init: function() {
        "use strict";
        var me = this;
        var oldWindows = [];

        this.control({
            'gamewindow': {
                afterrender: function(win) {
                    //Only override existing window if we've finished the game
                    //or if the window is not defined
                    if (!me.win || !me.win.game.active){
                        if (oldWindows.length !== 0){
                            for (var i =0; i !== oldWindows.length; i++){
                                oldWindows[i].close();
                            }
                        }
                        me.win = win;
                        win.setPosition(5,40);
                    } else {
                        win.setPosition(660,40);
                        //TODO FIX THIS HORRIBLE HACK!!!!
                        tryUntilSuccess(function(){
                            if (win.game === undefined || win.game.win === undefined){
                                return false;
                            }
                            if (win.flipped !== me.win.flipped){
                                return true;
                            }

                            win.flip();
                            tryUntilSuccess(function(){
                                try {
                                    me.refreshInfo(win.game);
                                    return true;
                                } catch (err) {
                                    return false;
                                }
                            });
                            return true;
                        });
                    }
                    oldWindows.push(win);

                    console.log('=================================');
                    console.log('afterrender');
                    console.log(win);
                    console.log(me.win);
                    console.log(me.win.el.id);
                    console.log(me);
                    console.log(win.getComponent('cboard').getId());
                    console.log(me.win.getComponent('cboard').getId());
                    console.log(me.win.getEl());
                    console.log('=================================');

                    // For some reason there is a JQuery error if the element
                    // is passed to ChessBoard, so pass the ID as a string
                    // instead
                    win.board = new ChessBoard(win.getComponent('cboard').getId(), {
                        draggable: true,
                        dragOffBoard: 'snapback',
                        showNotation: false,
                        onDragStart: this.onDragStart,
                        onDrop: this.onDrop,
                        moveSpeed: 10,
                        sparePieces: true,
                        win: win,
                        c: me,
                    });
                },
                afterlayout: function(win){
                },
                beforeclose: function(win) {
                    if (!win.game.active) {
                        return true;
                    }
                    if (win.game.amPlaying()) {
                        // XXX ask if player wants to resign
                        return false;
                    }
                    if (win.game.relation === mater.model.Game.OBSERVING_EXAMINED ||
                        win.game.relation === mater.model.Game.OBSERVING_PLAYED)
                    {
                        this.application.fireEvent('sendline', 'unobserve ' + win.game.gameID);
                        // don't actually close the window yet
                        return false;
                    }
                    if (win.game.relation === mater.model.Game.EXAMINING) {
                        this.application.fireEvent('sendline', 'unexamine');
                        return false;
                    }
                    return true;
                }
            },
            'panel[itemId=observersTab]': {
                activate: function(p) {
                    if (!p.loaded) {
                        this.application.fireEvent('sendline',
                            'allob ' + p.up('window').game.gameID,
                            function(lines) {
                                // remove "1 game displayed"
                                lines.pop();
                                p.update(lines.join('\n'));
                                p.loaded = true;
                            });
                    }
                }
            },
            'gamewindow #flipButton': {
                click: function(b) {
                    var win = b.up('window');
                    win.flip();
                    console.log(win);
                    me.refreshInfo(win.game);
                }
            },
            'gamewindow #back999Button': {
                click: function(b) {
                    me.application.fireEvent('sendline', 'back 999');
                }
            },
            'gamewindow #backButton': {
                click: function(b) {
                    me.application.fireEvent('sendline', 'back');
                }
            },
            'gamewindow #forwardButton': {
                click: function(b) {
                    me.application.fireEvent('sendline', 'forward');
                }
            },
            'gamewindow #forward999Button': {
                click: function(b) {
                    me.application.fireEvent('sendline', 'forward 999');
                }
            },
            'gamewindow #drawButton': {
                click: function() {
                    me.application.fireEvent('sendline', 'draw');
                }
            },
            'gamewindow #abortButton': {
                click: function() {
                    me.application.fireEvent('sendline', 'abort');
                }
            },
            'gamewindow #adjournButton': {
                click: function() {
                    me.application.fireEvent('sendline', 'adjourn');
                }
            },
            'gamewindow button#closeButton': {
                click: function(b) {
                    var win = b.up('window');
                    win.close();
                }
            },
            'gamewindow button#resignButton': {
                click: function() {
                    me.application.fireEvent('sendline', 'resign');
                }
            }
        });
    },

    onDragStart: function(from, piece, position, orientation) {
        /* jshint camelcase: false */
        "use strict";
        console.log('=========================');
        console.log('onDragStart');
        console.log(position);
        console.log(orientation);
        console.log(this.win.el.id);
        console.log('========================');
        var chess = this.win.game.chess;
        var relation = this.win.game.relation;
        if (!this.win.game.active || chess.game_over())
        {
            return false;
        }

        //if (relation === mater.model.Game.PLAYING_MYMOVE ||
        if (this.win.game.active ||
            relation === mater.model.Game.EXAMINING) {
            // playing or examining
            return true;
        }
        else {
            return false;
        }
    },

    onDrop: function(from, to, piece, position, oldPosition, orientation) {
        "use strict";

        var attempt = {
            from: from,
            to: to,
            piece: piece,
            promotion: 'q' // XXX.
        };

        console.log('=========================');
        console.log('onDrop');
        console.log(orientation);
        console.log(this.win.el.id);
        console.log(this.c.win.el.id);
        console.log(this);
        console.log('========================');

        if (this.win.el.id === this.c.win.el.id){
            return this.c.doMove(attempt);
        }
        return 'snapback';
    },

    remHighlights: function(from, to, color) {
        "use strict";
        var boardEl = $('#'+this.win.el.id);

        boardEl.find('.highlight-' + color)
          .removeClass('highlight-' + color);
    },

    remPremoveHighlights: function() {
        "use strict";
        if (typeof this.win.game.premove === 'undefined') {
            return;
        }

        var premove = this.win.game.premove;
        var from = premove.from;
        var to = premove.to;
        this.remHighlights(from, to, 'blue');
    },

    isLegal: function(attempt) {
        "use strict";

        function legalPiecePlacement() {
            return game.handHas(color, attempt.piece) &&
                   chess.get(attempt.to) === null;
        }

        var game = this.win.game;
        console.log("checking legality...");
        console.log(game.chess.turn());
        console.log(game.relation);
        console.log(mater.model.Game.PLAYING_MYMOVE);
        if (game.relation !== mater.model.Game.PLAYING_MYMOVE) {
            console.log("Not my turn!");
            //console.log("please.");
            return null;
        }
        var chess = game.chess,
            color = chess.turn();
        if (attempt.from === 'spare') {
            if (legalPiecePlacement()) {
                var piece = attempt.piece,
                    objPiece = {type: piece.charAt(1).toLowerCase(),
                                color: piece.charAt(0)},
                    destination = attempt.to;
                if (chess.dropPiece(objPiece, destination)) {
                    return {};
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return chess.move(attempt);
        }
    },

    doMove: function(attempt) {
        "use strict";

        var from = attempt.from,
            to = attempt.to,
            piece = attempt.piece,
            move = this.isLegal(attempt),
            relation = this.win.game.relation;

        console.log("relation : " +  relation);

        if (move === null &&
            relation === mater.model.Game.PLAYING_MYMOVE)
        {
            // The user is making an illegal move during his turn.
            // Tell the caller to snap the piece back to its original square.
            this.remPremoveHighlights();
            return 'snapback';
        } else if (move === null &&
                   relation !== mater.model.Game.PLAYING_MYMOVE)
        {
            // The user is doing a premove.
            //
            // Remove the previous premove.
            this.remPremoveHighlights();

            // Remember the premove.
            this.win.game.premove = attempt;

            // Mark the squares of the premove.
            var boardEl = $('#'+this.win.el.id);

            console.log('==========================');
            console.log(this.win.el.id);

            boardEl.find('.square-' + from)
                .addClass('highlight-blue')
                .addClass('highlight-premove');
            boardEl.find('.square-' + to)
                .addClass('highlight-blue')
                .addClass('highlight-premove');

            // Tell the caller to snap the piece back to its original square.
            return 'snapback';
        } else {
            // The user is making a legal move during his turn.
            var computerNotation;
            if (from === "spare") {
                piece = this.win.game.normalizeNotation(piece);
                computerNotation = piece + '@' + to;
            } else {
                computerNotation = from + '-' + to;
            }
            if (move.promotion) {
                computerNotation += '=' + move.promotion;
            }
            this.win.game.ply += 1;
            this.application.fireEvent('sendline', computerNotation);
            this.application.fireEvent('sound', 'move');

            // Remove the previous premove.
            this.remPremoveHighlights();
            this.win.game.premove = undefined;
        }
    },

    msToStr: function(ms) {
        "use strict";
        if (ms < 0) {
            var ret = this.msToStr(-ms);
            if (ret === '0:00') {
                // don't show -0:00
                return ret;
            }
            else {
                return '-' + ret;
            }
        }
        var totSecs = ms / 1000.0;
        var mins = Math.floor(totSecs / 60);
        var secs = Math.round(totSecs % 60);
        if (secs === 60) {
            mins += 1;
            secs = 0;
        }
        if (secs.toString().length === 1) {
            secs = '0' + secs;
        }
        return mins + ':' + secs;
    },

    refreshInfo: function(game) {
        // refresh most of the info about the game in the window
        "use strict";
        var bottomClock = game.win.down('#bottomClock').down('#time');
        var topClock = game.win.down('#topClock').down('#time');
        var topLag = game.win.down('#topClock').down('#lag');
        var bottomLag = game.win.down('#bottomClock').down('#lag');

        if (game.clockTimerId !== null) {
            window.clearTimeout(game.clockTimerId);
            game.clockTimerId = null;
        }
        topClock.removeCls('clockToMove');
        bottomClock.removeCls('clockToMove');

        if ((game.win.board.orientation() === 'black') !== game.win.flipped) {
            game.win.board.orientation(game.win.flipped ? 'black' : 'white');
        }
        console.assert((game.win.board.orientation() === 'black') === game.win.flipped);

        if (game.win.flipped) {
            // black on the bottom
            bottomClock.update(this.msToStr(game.getBlackTime()));
            topClock.update(this.msToStr(game.getWhiteTime()));
            if (!game.isExamined()) {
                bottomLag.update('Lag: ' + this.msToStr(game.lag[0]));
                topLag.update('Lag: ' + this.msToStr(game.lag[1]));
            }
        }
        else {
            // white on the bottom
            topClock.update(this.msToStr(game.getBlackTime()));
            bottomClock.update(this.msToStr(game.getWhiteTime()));
            if (!game.isExamined()) {
                topLag.update('Lag: ' + this.msToStr(game.lag[0]));
                bottomLag.update('Lag: ' + this.msToStr(game.lag[1]));
            }
        }

        var clockToMove;
        if (game.win.flipped) {
            clockToMove = (game.ply % 2 === 0) ? topClock: bottomClock;
        }
        else {
            clockToMove = (game.ply % 2 === 0) ? bottomClock : topClock;
        }
        clockToMove.addCls('clockToMove');
        if (game.clockIsTicking) {
            var time = (game.ply % 2 === 0) ? game.getWhiteTime() : game.getBlackTime();
            game.clockTimerId = Ext.defer(this.updateClock, time % 1000,
                this, [{game: game, ply: game.ply}]);
        }
    },

    update: function(data) {
        "use strict";
        var plyDiff = data.ply - data.game.ply,
            game = data.game,
            chess = game.chess,
            newMove = data.san;

        console.log("Updating chess board");
        console.log("game id: " + game.gameID);
        /*
        for (var key in game){
            console.log("game key: " + key  + " : " + game[key]);
        }
        */
        console.log("game  " + Object.keys(game));
        
        console.log("chess " + Object.keys(chess));
        console.log("newMove  " + newMove);


        if (!data.fen && plyDiff !== 1 && plyDiff !== 0) {
            console.log('got compressed update for game with plyDiff = ' + plyDiff);
            console.log('data.ply ' + data.ply + ', data.game.ply ' + data.game.ply);
            return;
        }
        if (plyDiff === 0) {
            // A move we made ourselves; board was refreshed; illegal move;
            // board setup; or wname/bname. Also a delta <d1> board can be
            // sent at the end of the game with plyDiff === 0
            if (data.fen) {
                if (data.fen.split(' ')[0] !== game.win.board.fen()) {
                    game.win.board.position(data.fen, false);
                }
                if (data.fen.split(' ')[0] !== game.chess.fen().split(' ')[0]) {
                    game.chess.load(data.fen);
                }
            }
        }

        // The case for a move that's a piece drop.
        else if (plyDiff === 1 && newMove.indexOf("@") >= 0) {
            var parsed = newMove.split("@"),
                destination = parsed[1].substring(0,2),
                piece = parsed[0].toLowerCase(),
                color = chess.turn(),
                objPiece = {type: piece,
                            color: color};

            // Don't check legality of piece drop. Assume server has done the
            // work of checking legality. So simply execute the piece drop.
            chess.dropPiece(objPiece, destination);
            game.win.board.position(chess.fen(), false);
            //game.win.board.put();

            this.application.fireEvent('sound', 'move');
        }

        // The case for a move that's not a piece drop.
        else if (plyDiff === 1 && newMove.indexOf("@") < 0) {
            console.log(data);

            // got a new move
            var mv = game.chess.move(newMove);

            // make the move even if we further update the position below,
            // so it gets animated
            game.win.board.move(mv.from + '-' + mv.to);
            if (mv.flags.search(/[epkq]/i) !== -1) {
                // en passant, promotion, or castling; update the board
                var fen = data.fen;
                if (!fen) {
                    // we did't get the current position, so use
                    // the one in our local model
                    fen = game.chess.fen();
                }
                game.win.board.position(fen, false);
            }
            this.application.fireEvent('sound', 'move');
        }
        else if (plyDiff < 0) {
            // back/takeback
            game.win.board.position(data.fen);
            game.chess.load(data.fen);
        }
        else if (plyDiff > 0) {
            // started observing after the start of a game or went
            // forward more than one step in an examined game
            game.win.board.position(data.fen);
            game.chess.load(data.fen);
        }

        game.ply = data.ply;
        game.lag[data.ply % 2] += data.lag;
        if (data.fen) {
            // We ignore the value of flip passed with the game update.
            // It is only used to initialize the board.
            game.relation = data.relation;
            game.blackTime = data.blackTime;
            game.whiteTime = data.whiteTime;
            console.assert(data.turn === 'W' || data.turn === 'B');
            console.assert((data.turn === 'W') === (data.ply % 2 === 0));
        }
        else {
            console.assert('remaining' in data);
            if (data.ply % 2) {
                game.whiteTime = data.remaining;
            }
            else {
                game.blackTime = data.remaining;
            }
            /* jshint camelcase: false */
            data.clockIsTicking = data.ply > 1 && !game.chess.game_over();
            if (game.relation === mater.model.Game.PLAYING_MYMOVE) {
                game.relation = mater.model.Game.PLAYING_OPPMOVE;
            }
            else if (game.relation === mater.model.Game.PLAYING_OPPMOVE) {
                game.relation = mater.model.Game.PLAYING_MYMOVE;
            }
        }
        game.clockIsTicking = data.clockIsTicking;
        if (game.clockIsTicking) {
            game.startClock();
        }

        this.refreshInfo(game);
        if (data.san !== 'none') {
            var moveTxt;
            if (data.ply % 2 === 0) {
                moveTxt = (data.ply / 2) + '... ';
            }
            else {
                moveTxt = ((data.ply - 1) / 2 + 1) + '. ';
            }
            moveTxt += data.san;
            if (!game.isExamined()) {
                moveTxt += ' (' + this.msToStr(data.elapsed) + ')';
            }
            game.win.showLastMove(moveTxt);
        }

        console.log(this.win.game.gameID);
        console.log(this.win.game);

        // Guo: After all the update events, attempt to execute premove.
        var premove;
        if (typeof data.game.premove !== 'undefined') {
            premove = data.game.premove;
            this.doMove(premove); // updates this.win.game.chess;
        }

        // TODO. Check if gametype is zh or bug.
        //this.updatePiecesInHand(game);
    },

    updateClock: function(data) {
        "use strict";
        var clock;
        /*if (!data.game.win) {
            console.log('window has gone away');
            return;
        }*/
        if (data.game.ply !== data.ply) {
            console.log('expected ply ' + data.ply + ' but current ply is ' + data.game.ply);
            return;
        }
        var time;
        var whitesTurn = (data.game.ply % 2 === 0);
        if (whitesTurn) {
            time = data.game.getWhiteTime();
            if (data.game.win.flipped) {
                clock = data.game.win.down('#topClock');
            }
            else {
                clock = data.game.win.down('#bottomClock');
            }
        }
        else {
            time = data.game.getBlackTime();
            if (data.game.win.flipped) {
                clock = data.game.win.down('#bottomClock');
            }
            else {
                clock = data.game.win.down('#topClock');
            }
        }
        if (!clock) {
            console.log('could not find clock');
            return;
        }
        // If the opponent's time ran out, then call flag on him.
        //console.log(data)
        var ourTurn = (data.game.relation === mater.model.Game.PLAYING_MYMOVE);
        if (time < 0 && data.game.active && !ourTurn) {
            this.application.fireEvent('sendline', 'flag');
        }
        clock.down('#time').update(this.msToStr(time));
        var ms = time % 1000;
        if (ms < 100) {
            ms += 1000;
        }
        data.game.clockTimerId = Ext.defer(this.updateClock, ms, this, [data]);
    },

    updatePiecesInHand: function(data) {
        "use strict";
        var win = data.win,
            boardId = win.getComponent('cboard').getId(),
            game = win.game;
        $('#' + boardId + ' .one-spare-piece').each(function(index){
            $(this).attr('data-piece'); // gives u the piece you're looking at.

            // First ensure that the spare pieces exist.
            if ($(this).find('.spare-piece-num').length === 0) {
                $(this).append('<span class="spare-piece-num" data-num="0">' +
                    '(0)</span>');
            }

            // Update the information based on the model.
            var color = $(this).attr('data-piece').charAt(0);
            var num = game.handNum(color, $(this).attr('data-piece'));
            $(this).find('.spare-piece-num').html(num);

            // Hide <span> if didn't find any pieces.
            if (num === 0) {
                $(this).css("visibility", "hidden");
            } else {
                $(this).css("visibility", "visible");
            }
        });
        $('.spare-pieces-wK').hide();
        $('.spare-pieces-bK').hide();
    }
});

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
