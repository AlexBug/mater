/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('mater.controller.Chat', {
    extend: 'Ext.app.Controller',
    views: ['Chat'],
    refs: [
        {selector: 'statusbar[id=chatStatus]', ref: 'chatStatus'},
        {selector: 'tabpanel[id=chatTabs]', ref: 'chatTabs'},
        {selector: 'textfield[id=chatInput]', ref: 'chatText'},
        {selector: 'chat', ref: 'chatWin'},
        {selector: 'mainmenu #showChat', ref: 'showchatcheckbox'},
    ],

    init: function() {
        "use strict";
        this.application.on({
            icsloggedin: function() {
                var c = this.getChatWin();
                c.show();
                // in case we previously disabled it
                this.getChatText().enable();
            },
            sentTell : function(m){
                var username = m[1];
                var chatTabs = this.getChatTabs();
                var tab = chatTabs.down('[name=user-' + username + ']');
                var txt = Ext.String.htmlEncode(m[2]);
                if (!tab) {
                    tab = chatTabs.add({
                        title: username,
                        name: 'user-' + username,
                        username: username,
                        closable: true,
                        cls: 'chatTab',
                        autoScroll: true,
                        tpl: '<span class="chatUsername">{name}</span>{tags}: {txt}<br />',
                        iconCls: 'newChat',
                        tplWriteMode: 'append'
                    });
                    chatTabs.setActiveTab(tab);
                }
                var html = Ext.String.htmlEncode(txt);
                html = mater.controller.Console.linkify(html);
                tab.update({
                    name: mater.username,
                    tags: '',
                    txt: html
                });
                if (tab.rendered) {
                    // scroll to bottom
                    tab.body.dom.scrollTop = 999999;
                    tab.setIconCls('newChat');
                }

            },
            privatetell: function(m) {
                var username = m[1];
                var chatTabs = this.getChatTabs();
                var tab = chatTabs.down('[name=user-' + username + ']');
                var txt = Ext.String.htmlEncode(m[3]);
                txt = mater.controller.Console.linkify(txt);
                if (!tab) {
                    tab = chatTabs.add({
                        title: username,
                        name: 'user-' + username,
                        username: username,
                        closable: true,
                        cls: 'chatTab',
                        autoScroll: true,
                        tpl: '<span class="chatUsername">{name}</span>{tags}: {txt}<br />',
                        iconCls: 'newChat',
                        tplWriteMode: 'append'
                    });
                    if (chatTabs.getActiveTab() === null) {
                        chatTabs.setActiveTab(tab);
                    }
                }
                tab.update({name: m[1], tags: m[2], txt: txt});
                if (tab.rendered) {
                    // scroll to bottom
                    tab.body.dom.scrollTop = 999999;
                    tab.setIconCls('newChat');
                }
                //chatTabs.body.dom.scrollTop = 999999;

                this.application.fireEvent('sound', 'tellreceived');
            },
            privatetold: function(line) {
                this.getChatStatus().setText(line);
            },
            icslostconnection: this.lostConnection,
            scope: this
        });
        this.control({
            'textfield#chatInput': {
                specialkey: function(ct, e) {
                    if (this.getChatWin().isHidden()) {
                        return;
                    }
                    if (ct.id !== 'chatInput') {
                        return;
                    }
                    switch (e.getKey()) {
                        case e.ENTER:
                            var tab = this.getChatTabs().getActiveTab();
                            if (tab) {
                                this.tell(tab, ct.getValue());
                                tab.setIconCls('');
                            }
                            ct.setValue('');
                            break;
                    }
                }
            },
            chat: {
                minimize: function() {
                    this.getShowchatcheckbox().setChecked(false);
                }
            },
            tabpanel: {
                tabchange: function(tp, newtab, oldtab) {
                    newtab.setIconCls('');
                    if (oldtab) {
                        oldtab.setIconCls('');
                    }
                    this.getChatStatus().setText('');
                }
            }
        });
    },

    tell: function(tab, txt) {
        "use strict";
        var me = this;
        var toName = tab.username;
        tab.body.dom.scrollTop = 999999;
        this.application.fireEvent('sendline', 'tell ' + toName + ' ' + txt,
            function(lines) {
                me.getChatStatus().setText(lines[lines.length - 1]);
            });
        this.application.fireEvent('sound', 'tellsent');
    },

    lostConnection: function() {
        "use strict";
        var c = this.getChatText();
        c.disable();
    }
});

/* vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
 */
