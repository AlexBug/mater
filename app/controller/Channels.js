/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

/* global console: false */

Ext.define('mater.controller.Channels', {
    extend: 'Ext.app.Controller',
    views: ['Channels'],
    models: ['Channel'],
    stores: ['ChannelList'],
    refs: [
        {selector: 'statusbar[id=channelStatus]', ref: 'channelStatus'},
        {selector: 'tabpanel[id=channelTabs]', ref: 'channelTabs'},
        {selector: 'textfield[id=channelInput]', ref: 'channelText'},
        {selector: 'window[id=channelWindow]', ref: 'channelWin'},
        {selector: 'mainmenu #showChannels', ref: 'showchannelscheckbox'},
    ],

    init: function() {
        "use strict";
        var me = this;
        me.channelList = {};
        this.getChannelListStore().each(function(record, i) {
            me.channelList[record.data.id] = record.data.desc;
        });
        this.application.on({
            icsloggedin: function() {
                var c = this.getChannelWin();
                c.show();
                // in case we previously disabled it
                this.getChannelText().enable();

                // Alex Guo: show console if checkbox was checked.
                if (this.getShowchannelscheckbox().checked) {
                    this.getChannelWin().show();
                } else {
                    this.getChannelWin().hide();
                }
            },
            channeltell: function(data) {
                var newChatCls = data.username === mater.username ?
                    '' : 'newChat';
                var tab = this.getTab(data.chname, newChatCls);
                data.txt = Ext.String.htmlEncode(data.txt);
                data.txt = mater.controller.Console.linkify(data.txt);
                tab.update(data);
                if (tab.rendered) {
                    // scroll to bottom
                    tab.body.dom.scrollTop = 999999;
                    tab.setIconCls(newChatCls);
                }
            },
            icslostconnection: this.lostConnection,
            scope: this
        });
        this.control({
            'textfield#channelInput': {
                specialkey: function(ct, e) {
                    if (this.getChannelWin().isHidden()) {
                        return;
                    }
                    if (ct.id !== 'channelInput') {
                        return;
                    }
                    switch (e.getKey()) {
                        case e.ENTER:
                            var tab = this.getChannelTabs().getActiveTab();
                            if (tab) {
                                tab.setIconCls('');
                                this.tell(tab, ct.getValue());
                            }
                            ct.setValue('');
                            break;
                    }
                }
            },
            channels: {
                minimize: function() {
                    this.getShowchannelscheckbox().setChecked(false);
                }
            },
            tabpanel: {
                tabchange: function(tp, newtab, oldtab) {
                    if (newtab.title in this.channelList) {
                        newtab.tab.setText(newtab.title + ': ' + this.channelList[newtab.title]);
                    }
                    newtab.setIconCls('');
                    if (oldtab) {
                        oldtab.setIconCls('');
                        oldtab.tab.setText(oldtab.title);
                    }
                    this.getChannelStatus().setText('');
                    Ext.defer(function() {
                        this.getChannelText().focus();
                    }, 100, this);
                }
            },
            'mainmenu [itemId=askForHelp]': {
                click: function() {
                    var channelTabs = this.getChannelTabs();
                    var input = this.getChannelText();
                    var chname;
                    if (this.application.getController('Ics').amGuest) {
                        chname = '4';
                    }
                    else {
                        chname = '1';
                    }
                    var tab = this.getTab(chname, '');
                    channelTabs.setActiveTab(tab);
                    // unfortunately it seems there is no straightforward
                    // way to set a placeholder value
                    /*this.getChannelText().el.set({
                        placeholder: 'Type your question here...'
                    });*/
                    input.setValue('Type your question here...');
                    input.focus();
                    input.selectText();
                }
            }
        });
    },

    // find the tab with the given name, or create it if it doesn't exist
    getTab: function(name, newChatCls) {
        "use strict";
        var channelTabs = this.getChannelTabs();
        var tab = channelTabs.down('[name=chan-' + name + ']');
        var sortIndex;
        if (!tab) {
            var tellFunc;
            var chNum = parseInt(name, 10);
            if (isNaN(chNum)) {
                if (name === 'Shouts') {
                    tellFunc = function(txt) {
                        return 'shout ' + txt;
                    };
                    sortIndex = 1;
                }
                else if (name === 'C-shouts') {
                    tellFunc = function(txt) {
                        return 'cshout ' + txt;
                    };
                    sortIndex = 2;
                }
                else if (name === 'FTL-admins') {
                    tellFunc = function(txt) {
                        return 'xtell teamleague! ' + txt;
                    };
                    sortIndex = 3;
                }

                else {
                    console.log('unrecognized channel ' + name);
                    return;
                }
            }
            else {
                // channel has a number
                sortIndex = chNum + 5;
                if (mater.serverType === 'fics' && chNum > 255) {
                    tellFunc = function(txt) {
                        return 'xtell channelbot! tell ' + name + ' ' + txt;
                    };
                }
                else {
                    tellFunc = function(txt) {
                        return 'xtell ' + name + ' ' + txt;
                    };
                }
            }
            for (var i = 0; i < channelTabs.items.items.length; i++) {
                if (sortIndex < channelTabs.items.items[i].sortIndex) {
                    break;
                }
            }
            tab = channelTabs.insert(i, {
                title: name,
                name: 'chan-' + name,
                tellFunc: tellFunc,
                sortIndex: sortIndex,
                closable: true,
                autoScroll: true,
                cls: 'chatTab',
                tpl: '<span class="chatUsername">{username}</span>{tags}: {txt}<br />',
                iconCls: newChatCls,
                tplWriteMode: 'append'
            });
            if (channelTabs.getActiveTab() === null) {
                channelTabs.setActiveTab(tab);
            }
        }
        return tab;
    },

    tell: function(tab, txt) {
        "use strict";
        var me = this;
        // We don't show our own tell until we get it back from the server
        this.application.fireEvent('sendline', tab.tellFunc(txt),
            function(lines) {
                me.getChannelStatus().setText(lines[lines.length - 1]);
            });
    },

    lostConnection: function() {
        "use strict";
        var c = this.getChannelText();
        c.disable();
    }
});

/* vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
 */
