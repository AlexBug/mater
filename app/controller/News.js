/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('mater.controller.News', {
    extend: 'Ext.app.Controller',

    refs: [
        {ref: 'newsWin', selector: 'news'},
        {ref: 'shownewscheckbox', selector: 'mainmenu #showNews'}
    ],

    views: ['News'],

    init: function() {
        "use strict";
        this.control({
            newsWin: {
                show: function() {
                    this.getShownewscheckbox().setChecked(true);
                }
            }
        });
        this.application.on({
            icsloggedin: function() {
                if (this.getShownewscheckbox().checked) {
                    this.getNewsWin().show();
                } else {
                }
            },
            news: function(txt) {
                var nt = this.getNewsWin().down('#newsText');
                txt = Ext.String.htmlEncode(txt);
                txt = mater.controller.Console.linkify(txt);
                nt.update({html: txt});
                if (nt.el) {
                    nt.el.scroll('top', 999999, {duration: 10*txt.length});
                }
            },
            scope: this
        });
    }
});

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
