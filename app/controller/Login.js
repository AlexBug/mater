/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

Ext.define('mater.controller.Login', {
    extend: 'Ext.app.Controller',

    views: [
        'LoginWindow', 'LoginForm'
    ],

    refs: [
        {
            ref: 'loginwindow',
            selector: 'loginwindow'
        },
        {
            ref: 'loginform',
            selector: 'loginform'
        },
        {
            ref: 'username',
            selector: 'textfield[name=username]'
        },
        {
            ref: 'passwd',
            selector: 'textfield[name=password]'
        },
    ],

    init: function() {
        "use strict";
        this.control({
            'loginwindow': {
                show: function() { this.getUsername().focus(); }
            },
            'loginform button[action=login]': {
                click: function() { this.logIn(false); }
            },
            'loginform button[action=guestlogin]': {
                click: function() {
                    //this.getUsername().setValue('guest');
                    this.getUsername().allowBlank = true;
                    this.getPasswd().allowBlank = true;
                    this.getUsername().disabled = true;
                    this.getPasswd().disabled = true;
                    this.logIn(true);
                }
            },

            'loginform textfield[name=username]': {
                specialkey: function(f, e){
                    switch (e.getKey()) {
                        case e.ENTER:
                            this.getPasswd().focus();
                            break;
                        default:
                            return false;
                    }
                    return true;
                }
            },

            'loginform textfield[name=password]': {
                specialkey: function(f, e){
                    switch (e.getKey()) {
                        case e.ENTER:
                            this.logIn(false);
                            break;
                        default:
                            return false;
                    }
                    return true;
                }
            }
        });
        this.application.on({
            icsloggingin: {
                fn: function() {
                    //this.hideMask(); // XXX
                    this.showMask('Logging in....');
                },
                priority: 20
            },
            icsloginfailed: {
                fn: function() {
                    this.hideMask();
                },
                priority: -10
            },
            loadingsettings: function() {
                this.showMask('Loading settings....');
            },
            icsloggedin: function() {
                this.hideMask();
                // Closing the window seems to work but causes Ext JS
                // to give an error when resizing the browser, so hide
                // it instead.
                this.getLoginwindow().hide();
            },
            scope: this
        });
    },

    logIn: function(asGuest) {
        "use strict";

        // Some Simple DRM.
        var invalidDRM;
        invalidDRM = false;
        //if (window.location.origin.indexOf("le") < 0) {
            //invalidDRM = true;
        //} else {
            //invalidDRM = false;
        //}

        var username = this.getUsername().getValue();
        var passwd;
        if (asGuest) {
            if (username.length < 3) {
                username = 'guest';
            }
            passwd = '';
        }
        else {
            this.getUsername().allowBlank = false;
            passwd = this.getPasswd().getValue();
            if (!this.getUsername().validate() || !this.getPasswd().validate()) {
                return;
            }
        }
        this.showMask('Connecting to server....');

        // Some simple DRM.
        if (invalidDRM) {
            return;
        }

        this.application.fireEvent('icsconnect', username, passwd);
    },

    showMask: function(txt) {
        "use strict";
        if (typeof(this.connectingMask) !== 'undefined') {
            this.connectingMask.hide();
        }
        this.connectingMask = new Ext.LoadMask({
            target: this.getLoginform(),
            msg: txt
        });
        this.connectingMask.show();
    },
    hideMask: function() {
        "use strict";
        this.connectingMask.hide();
    }
});

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
