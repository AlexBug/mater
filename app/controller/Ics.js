/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

/* global window, SockJS, console, Timeseal */

Ext.define('mater.controller.Ics', {
    extend: 'Ext.app.Controller',

    ivars: {
        compressmove: 1,
        audiochat: 0,
        seekremove: 0,
        defprompt: 1,
        lock: 1,
        startpos: 1,
        block: 1,
        gameinfo: 1,
        xdr: 0, // ignored
        pendinfo: 0, // XXX change to 1 eventually
        graph: 0,
        seekinfo: 0, // XXX change to 1 eventually
        extascii: 0,
        nohighlight: 1,
        vthighlight: 0,
        showserver: 0,
        pin: 0,
        ms: 1,
        pinginfo: 0,
        boardinfo: 0,
        extuserinfo: 0,
        seekca: 0,
        showownseek: 1,
        premove: 0,
        smartmove: 0,
        movecase: 0,
        suicide: 0,
        crazyhouse: 0,
        losers: 0,
        wildcastle: 0,
        fr: 0,
        nowrap: 1,
        allresults: 0,
        obsping: 0, // ignored
        singleboard: 0
    },
    sock: null,
    loginState: 'init',
    readText: '',
    consoleLines: [],
    gameData: null,

    controllers: ['Login'],

    init: function() {
        "use strict";
        mater.serverType = 'fics';

        if (mater.serverType === 'fics') {
            this.timeseal = true;
            this.zipseal = false;
            // fics proxy
            this.sockjsUrl = 'https://' + window.location.host +
                ':8081/';
            this.lineRe = /^(.*?)\n\r/g;
        }
        else if (mater.serverType === 'fatics') {
            this.timeseal = false;
            this.zipseal = true;
            // FatICS websocket port
            this.sockjsUrl = 'https://' + window.location.host +
                ':8080/';
            this.lineRe = /^(.*?)\r\n/g;
        }
        else {
            window.alert('unknown ICS type');
            console.assert(false);
        }
        this.sockjsUrl = "http://bughouseclub.com:5011";
        console.assert(!(this.zipseal && this.timeseal));

        this.application.on({
            icsconnect: this.connect,
            icsquit: function() {
                if (this.loginState === 'LoggedIn') {
                    this.sendLine('quit');
                }
            },
            rematchGame: function(){
                this.sendLine('rematch');
            },
            matchGame: function(matchDetails){
                console.log('TRYING TO MATCH');
                var variant = matchDetails.variant;
                if (variant === 'chess' || 'Chess'){
                    variant = '';
                }
                var command = 'match ' + matchDetails.name + ' ' +
                    matchDetails.initialTime + ' ' +
                    matchDetails.increment + ' ' +
                    variant;
                console.log(command);
                this.sendLine(command);
            },
            seek: function(time){
                console.log('Seeking for time control: ' + time);
                this.sendLine('seek ' + time);
            },
            partnerRequest: function(reqDetails){
                this.sendLine('partner ' + reqDetails.partnerName);
            },
            sendline: this.sendLine,
            ping: this.sendPing,
            scope: this
        });

    },

    connect: function(user, pass) {
        "use strict";
        if (this.sock !== null) {
            window.alert('already connected');
            return;
        }
        this.username = user;
        this.passwd = pass;
        this.loginState = 'Connecting';
        this.sock = new SockJS(this.sockjsUrl);
        this.connectTimeoutId = Ext.defer(this.connectTimeout, 15000, this);
        this.sock.onopen = this.connOpen;
        this.sock.onmessage = this.dataReceived;
        this.sock.onerror  = this.connError;
        this.sock.onclose = this.connClose;
        this.sock.c = this;
    },

    connOpen: function(e) {
        "use strict";
        var c = this.c;
        window.clearTimeout(c.connectTimeoutId);
        c.loginState = 'Connected';
        c.application.fireEvent('icsloggingin');
    },

    connectionError: function() {
        "use strict";
        Ext.MessageBox.show({
            title: 'Connection error',
            msg: 'There was an error connecting to server.\nPlease notify an admin.',
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.ERROR
        });
    },

    loginFailed: function() {
        "use strict";
        this.loginState = 'Closed';
        window.clearTimeout(this.connectTimeoutId);
        this.application.fireEvent('icsloginfailed');
        if (this.sock) {
            this.sock.close();
            this.sock = null;
        }
    },

    connClose: function(e) {
        "use strict";
        var c = this.c;
        if (c.loginState === 'Connecting') {
            window.clearTimeout(c.connectTimeoutId);
            c.connectionError();
            c.application.fireEvent('icsloginfailed');
        }
        else if (c.loginState === 'Login' || c.loginState === 'LoggedIn') {
            Ext.Msg.alert('Lost connection', 'Connection to server lost');
        }
        if (c.loginState === 'Login') {
            c.loginFailed();
        }
        if (c.sock) {
            c.sock.close();
            c.sock = null;
        }
        if (c.lagTimer) {
            window.clearTimeout(c.lagTimer);
            c.lagTimer = null;
        }
        // XXX maybe this should be named to icsloggedout
        c.application.fireEvent('icslostconnection');
        c.loginState = 'Closed';
        window.onbeforeunload = null;
    },

    connError: function(e) {
        "use strict";
        console.log('conn error');
    },

    connectTimeout: function() {
        "use strict";
        console.log('timeout');
        //this.application.fireEvent('icslogintimeout');
        this.connectionError();
    },

    // Replace characters that FICS can't handle. This is intended to
    // be compatible with what Raptor calls Maciejg format.
    toAscii: function(line) {
        "use strict";
        var out = [], outPos = 0;
        for (var i = 0; i < line.length; i++) {
            var c = line.charCodeAt(i);
            if (c >= 0x20 && c < 0x7f) {
                out[outPos++] = String.fromCharCode(c);
            }
            // why does Raptor filter out chars less than
            // 256 here?
            else if (c >= 0x7f) {
                out[outPos++] = '&#x' + c.toString(16) + ';';
            }
            else {
                // The character is removed. Should we
                // notify the user?
                console.log('ignoring ' + c);
            }
        }
        return out.join('');
    },



    // block mode sequence number and callback functions
    // sequence number 1 is used for all non-callback commands
    callbacks: [null],
    sequenceNum: 2,
    sendLine: function(line, callback) {
        "use strict";
        console.log("SENDING LINE: " + line);
        console.assert(line[line.length - 1] !== '\n');
        if (!this.sock) {
            // can happen e.g. if the user clicks on a linkified
            // command after logging out
            return;
        }
        var youTell = /tell ([A-Za-z]+.*?) (.*)/ ;
        var m = youTell.exec(line);
        console.log(m);
        if (m){
            this.application.fireEvent('sentTell', m);

        }
        if (mater.serverType !== 'fatics') {
            line = this.toAscii(line);
        }

        // Alex Guo: This all "callback" idea with sequence numbers is buggy,
        // overambitious, and unnecessarily overcomplicating.
        // 
        //if (this.loginState === 'LoggedIn') {
        //    var seqNum;
        //    if (typeof callback !== "function") {
        //        // used for all commands without callbacks
        //        seqNum = 1;
        //    }
        //    else {
        //        seqNum = this.sequenceNum++;
        //        if (seqNum < this.callbacks.length && this.callbacks[seqNum]) {
        //            window.alert('sequence number overflow');
        //            return;
        //        }
        //        if (this.sequenceNum === 10) {
        //            this.sequenceNum = 2;
        //        }
        //        this.callbacks[seqNum] = callback;
        //    }
        //    line = seqNum + ' ' + line;
        //}
        //else if (typeof callback !== "undefined") {
        //    window.alert('callback not allowed in ' + this.loginState + ' state');
        //    return;
        //}

        if (this.zipseal) {
            // zipseal does not need the newline
            line = Timeseal.zipsealEncode(line);
        }
        else if (this.timeseal) {
            line = Timeseal.timesealEncode(line);
        }
        else {
            line = line + '\r\n';
        }
        this.sock.send(line);
    },

    sendPing: function() {
        "use strict";
        console.log("sending ping...");
        var line = "";
        if (this.timeseal) {
            line = Timeseal.timesealEncode(Timeseal.pongReply);
        }
        // TODO. add support for normal telnet and zipseal protocols.
        this.sock.send(line);
    },

    loginRe: /^login: /,
    dataReceived: function(e) {
        "use strict";
        var m;
        var data = e.data;
        var c = this.c;
        var decoded = data;
        if (c.zipseal) {
            decoded = Timeseal.zipsealDecode(decoded, this);
        }
        else if (c.timeseal) {
            decoded = Timeseal.timesealDecode(decoded, this);
        }

        c.readText += decoded;
        c.lineRe.lastIndex = 0;

        while (1) {
            m = c.lineRe.exec(c.readText);
            if (!m) {
                break;
            }
            c.readText = c.readText.substr(c.lineRe.lastIndex);
            c.lineRe.lastIndex = 0;
            console.assert(('lineReceived' + c.loginState) in c);
            c['lineReceived' + c.loginState](m[1]);
        }
        if (c.consoleLines.length > 0) {
            c.application.fireEvent('gotlines', c.consoleLines);
            c.consoleLines = [];
        }

        // this is not done in lineReceived because there is no newline
        // after login:
        if (c.loginState === 'Connected' && c.readText) {
            m = c.loginRe.exec(c.readText);
            if (m) {
                c.loginState = 'SentIvars';
                var ivarStr = '%b';
                for (var iv in c.ivars) {
                    if (c.ivars.hasOwnProperty(iv)) {
                        ivarStr += c.ivars[iv];
                    }
                }
                c.readText = '';
                if (c.zipseal) {
                    c.sendLine(Timeseal.zipsealHello());
                }
                else if (c.timeseal) {
                    c.sendLine(Timeseal.timesealHello());
                }
                // Alex Guo: Do not bother sending string beginning with '%b'.
                //c.sendLine(ivarStr);

                // Alex Guo: Need to send login information at this point as well.
                c.sendLine(c.username);
                c.loginState = 'SentUsername';
                return;
            }
        }
    },

    lineReceivedConnected: function(line) {
        // Don't do anything with the lines initially sent by the server,
        // just wait for dataReceived() to see the login: line
    },

    ivarsSetRe: /^#Ivars set\./,
    lineReceivedSentIvars: function(line) {
        // Alex Guo: BICS doesn't support Ivars, so username is sent
        // when we send the timeseal hello.
        //
        //"use strict";
        //
        //var m = this.ivarsSetRe.exec(line);
        //if (m) {
        //    this.sendLine(this.username);
        //    this.loginState = 'SentUsername';
        //    return;
        //}
    },


    passwdRe: / is a registered name\./,
    guestLoginRe: /^Logging you in as Guest"|^Press return to enter the server as /,
    alreadyLoggedInRe: /is already logged in/,
    lineReceivedSentUsername: function(line) {
        "use strict";
        var m = this.passwdRe.exec(line);
        if (m) {
            if (this.passwd === '') {
                Ext.MessageBox.show({
                    title: 'Guest login error',
                    msg: 'Username is registered',
                    buttons: Ext.MessageBox.OK,
                    icon:Ext.MessageBox.ERROR
                });
                this.loginFailed();
                return;
            }
            else {
                this.amGuest = false;
                this.loginState = 'SentPasswd';
                line = '';
                this.sendLine(this.passwd);
                return;
            }
        }

        // check if the server offered to enter as a guest
        m = this.guestLoginRe.exec(line);
        if (m) {
            if (this.passwd !== '') {
                Ext.MessageBox.show({
                    title: 'Login error',
                    msg: 'Username does not exist',
                    buttons: Ext.MessageBox.OK,
                    icon:Ext.MessageBox.ERROR
                });
                this.loginFailed();
                return;
            }
            else {
                // continue logging in
                this.loginState = 'SentPasswd';
                this.amGuest = true;
                line = '';
                this.sendLine('');
                return;
            }
        }

        m = this.alreadyLoggedInRe.exec(line);
        if (m) {
            Ext.MessageBox.show({
                title: 'Login error',
                msg: 'Username is already logged in',
                buttons: Ext.MessageBox.OK,
                icon:Ext.MessageBox.ERROR
            });
            this.loginFailed();
        }
    },

    loggedInRe: /^\*\*\*\* Starting FICS session as ([^( ]+)/,
    loggedInRe2: /^\*\*\*\* Starting BICS session as ([^( ]+)/,
    failedLoginRe: /Invalid password/,
    lineReceivedSentPasswd: function(line) {
        "use strict";
        var m = this.loggedInRe.exec(line);
        var n = this.loggedInRe2.exec(line);
        // Alex Guo: The next 50 lines of code depends on m, so i just
        // set m to the result of n, if n was successful.
        if (n) {
            m = n;
        }
        if (m) {
            // logged in
            // probably doesn't make any difference but for paranoia
            this.passwd = '';
            this.username = m[1];
            mater.username = m[1];
            window.onbeforeunload = function() { return "Leave FatICS?"; };
            window.document.title += ' - ' + mater.icsName;

            // load settings
            if (this.amGuest) {
                this.loadSettings(Ext.create('mater.model.Settings', {
                    /*proxy: {
                        type: 'sessionstorage',
                        id: 'settings'
                    }*/
                }));
            }
            else {
                this.application.fireEvent('loadingsettings', m[1]);
                mater.model.Settings.load(
                    this.username,
                    {callback: this.loadSettings, scope: this});
            }
            return;
        }

        m = this.failedLoginRe.exec(line);
        if (m) {
            Ext.MessageBox.show({
                title: 'Login error',
                msg: 'Invalid username or password<br /><a href="http://www.freechess.org/cgi-bin/Utilities/requestPassword.cgi">Forgot your password?</a>',
                buttons: Ext.MessageBox.OK,
                icon:Ext.MessageBox.ERROR
            });
            this.loginFailed();
            return;
        }
    },

    blockStartRe: /^\x15(\d+)\x16(.*)$/,
    blockCommandRe: /^(\d+)\x16(.*)$/,
    blockEndRe: /^(.*)\x17$/,
    // Raptor's "Maciejg format" entities
    maciejgRe: /&#x([0-9a-f]+);/g,
    block: null,
    // ugly hack to separate out the news at the beginning of a session
    readingInitialNews: true,
    initialNews: [],
    tellRe: /^([A-Za-z]+)(\(.*?\))* tells you: (.*)/,
    channelRe: /^:?([A-Za-z]+)(\(.*?\))*\((\d+)\): (.*)/,
    //channelToldRe: /^\(told \d+ (?:player|admin)s? in channel \d+\)/,
    shoutRe: /^([A-Za-z]+)(\(.*?\))* (shouts|c-shouts): (.*)/,
    itShoutRe: /^--> ([A-Za-z]+)(\(.*?\)).*/,
    ftlAdminRe: /^:([A-Za-z]+)(\(.*?\))*\(FTL-admins\): (.*)/,
    fwdTellRe: /^Fwd tell: [A-Za-z]+ told [A-Za-z]+: (.+)/,
    //toldRe: /^\(told ([A-Za-z]+)(, who .*?)?\)$/,
    //creating0Re: /^Creating: (.*?) \((.*?)\) (.*?) \((.*?)\) ((?:rated|unrated) .*)/,
    gameinfoRe: /^<g1> (\d+) p=(\d+) t=(\w+) r=(\d+) u=(\d+),(\d+) it=(\d+),(\d+) i=(\d+),(\d+) pt=(\d+) rt=(\d+\w?),(\d+\w?) ts=(\d+),(\d+) m=(\d+) n=(\d+)/,
    handPiecesRe: /^<b1> game (\d+) white \[((\w+)|())\] black \[((\w+)|())\]/,
    // should these be shown in the game chat window?
    //creatingRe: /^{Game (\d+) \((.*?) vs. (.*?)\) Creating (.*?)\.}/,
    //obsRe: /^Game (\d+): (.*?) \((.*?)\) (.*?) \((.*?)\) ((?:rated|unrated) .*)/,
    gameMsgRe: /Game (\d+): (.*)/,
    commentRe: /([A-Za-z]+)(\(.*\))*\[(\d+)\]( (?:says|kibitzes|whispers): .*)/,
    examineRe: /^Starting a game in examine \(scratch\) mode\./,
    bsetupRe: /^Entering setup mode\./,
    style12Re: /^<12> ((?:[-pPnNbBrRqQkK]{8} ){8})([WB]) (-?\d) ([01]) ([01]) ([01]) ([01]) (\d+) (\d+) (\w+) (\w+) (-?\d+) (\d+) (\d+) (\d+) (\d+) (-?\d+) (-?\d+) (\d+) ([^ ]+) \((\d+:\d+\.\d+)\) ([^ ]+) ([01]) ([01]) (\d+)/,
    compressmoveRe: /^<d1> (\d+) (\d+) ([^ ]+) (\w+) (\d+) (\d+) (\d+)/,
    style12DashRe: /(-+)/g,
    spaceRe: / /g,
    gameEndRe: /^\{Game (\d+) \([A-Za-z]+ vs. [A-Za-z]+\) (.*?)\} (1-0|0-1|1\/2-1\/2|\*)/,
    unexRe: /^You are no longer examining game (\d+)\./,
    unobRe: /^Removing game (\d+) from observation list\./,
    announceRe: /^ +\*\*.*?\*\* .*/,
    // messages we have received about games before we have created the
    // corresponding Game object
    pendingGameMessages: [],
    pingRe: /^\[G\]/,

    //Message telling us the game ID our partner is playing on
    partnerGameRe: /^Your partner is playing game (\d+) (.*)/,

    lineReceivedLoggedIn: function(line) {
        "use strict";
        var game, gameID, m;
        line = line.replace(this.maciejgRe, function($0, $1) {
            if ($1.length <= 6) {
                var val = parseInt($1, 16);
                if (val >= 0x7f) {
                    return String.fromCharCode(val);
                }
            }
            return $1;
        });

        var lastWasPrompt = false;
        while (line.substr(0, 6) === 'fics% ') {
            // ignore prompt lines
            line = line.substr(6);
            lastWasPrompt = true;

            if (this.readingInitialNews) {
                this.readingInitialNews = false;
                this.application.fireEvent('news',
                    this.initialNews.join("\n"));
            }
        }

        // Always give the console what we rcved from the server.
        this.consoleLines.push(line);

        if (this.readingInitialNews) {
            this.initialNews.push(line);
            return;
        }

        if (this.block === null) {
            m = this.blockStartRe.exec(line);
            if (m) {
                this.block = {
                    seq: parseInt(m[1], 10),
                    lines: []
                };
                if (this.block.seq in this.callbacks &&
                    this.callbacks[this.block.seq])
                {
                    this.block.callback = this.callbacks[this.block.seq];
                    this.callbacks[this.block.seq] = null;
                }
                line = m[2];
            }
        }

        if (this.block !== null) {
            m = this.blockCommandRe.exec(line);
            if (m) {
                this.block.code = parseInt(m[1], 10);
                line = m[2];
            }

            if (!('code' in this.block)) {
                console.log('failed to find block code for sequence ' +
                    this.block.seq);
                return;
            }

            m = this.blockEndRe.exec(line);
            if (m) {
                var block = this.block;
                this.block = null;
                line = m[1];
                if (line !== '') {
                    block.lines.push(line);
                }
                if (block.callback) {
                    block.callback(block.lines);
                    return;
                }
                if (block.code === mater.blockCodes.FINGER) {
                    // eventually we should only display the finger
                    // window via a callback
                    this.application.fireEvent('showfinger', block.lines);
                }
                if (line === '') {
                    return;
                }
            }
            else {
                this.block.lines.push(line);
            }
        }

        if (line.charCodeAt(0) === 0x07) {
            // bell
            this.application.fireEvent('bell');
            line = line.substr(1);
        }
        if (lastWasPrompt && line.length === 0) {
            // ignore lines that are only prompts
            return;
        }

        m = this.partnerGameRe.exec(line);
        if (m){
            //Automatically observe the partner's board
            this.application.fireEvent('sendline', 'observe ' + m[1]);
        }

        m = this.tellRe.exec(line);
        if (m) {
            this.application.fireEvent('privatetell', m);
            return;
        }

        m = this.channelRe.exec(line);
        if (m) {
            this.application.fireEvent('channeltell', {
                username: m[1],
                tags: m[2],
                chname: m[3],
                txt: m[4]
            });
            return;
        }

        m = this.shoutRe.exec(line);
        if (m) {
            this.application.fireEvent('channeltell', {
                username: m[1],
                tags: m[2],
                chname: Ext.String.capitalize(m[3]),
                txt: m[4]
            });
            return;
        }

        m = this.itShoutRe.exec(line);
        if (m) {
            this.application.fireEvent('channeltell', {
                username: m[1],
                tags: m[2],
                chname: 'Shouts',
                txt: m[0]
            });
            return;
        }

        m = this.ftlAdminRe.exec(line);
        if (m) {
            this.application.fireEvent('channeltell', {
                username: m[1],
                tags: m[2],
                chname: 'FTL-admins',
                txt: m[3]
            });
            return;
        }

        m = this.pingRe.exec(line);
        if (m) {
            this.application.fireEvent('ping', {});
        }

        m = this.gameinfoRe.exec(line);
        if (m) {
            // sent for played and observed games
            console.assert(!this.gameData);
            this.gameData = {};
            this.gameData.gameID = parseInt(m[1], 10);
            this.gameData.isPrivate = parseInt(m[2], 10);
            this.gameData.type = m[3];
            this.gameData.isRated = parseInt(m[4], 10);
            this.gameData.whiteIsGuest = parseInt(m[5], 10);
            this.gameData.blackIsGuest = parseInt(m[6], 10);
            this.gameData.initialTime = parseInt(m[7], 10) / 60;
            this.gameData.inc = parseInt(m[8], 10);
            this.gameData.linkedGame = parseInt(m[11], 10);
            this.gameData.whiteRating = m[12];
            this.gameData.blackRating = m[13];
            if (this.gameData.whiteRating === '0') {
                this.gameData.whiteRating = this.gameData.whiteIsGuest ? '++++' : '----';
            }
            if (this.gameData.blackRating === '0') {
                this.gameData.blackRating = this.gameData.blackIsGuest ? '++++' : '----';
            }
            this.gameData.whiteHasTimeseal = parseInt(m[14]);
            this.gameData.blackHasTimeseal = parseInt(m[15]);
            this.gameData.desc = '';
            if (this.gameData.isPrivate) {
                this.gameData.desc += 'private ';
            }
            if (this.gameData.isRated) {
                this.gameData.desc += 'rated ';
            }
            else {
                this.gameData.desc += 'unrated ';
            }
            this.gameData.desc += this.gameData.type + ' ' + this.gameData.initialTime  + '+' + this.gameData.inc;
            return;
        }

        m = this.handPiecesRe.exec(line);
        // TODO: check why handPiecesRe also matches with [G]...
        // shouldn't be happening.
        if (m) {
            var gamenum = m[1],
                whiteHand = m[3],
                blackHand = m[5];
            gameID = parseInt(gamenum, 10);

            console.log('======================');
            console.log(line);
            console.log(m);

            if (whiteHand === undefined) {
                whiteHand = "";
            }
            if (blackHand === undefined) {
                blackHand = "";
            }

            if (gamenum !== undefined) {
                if (mater.model.Game.byID[gameID] !== undefined) {
                    game = mater.model.Game.byID[gameID];
                    game.whiteHand = whiteHand;
                    game.blackHand = blackHand.toLowerCase();
                    this.application.getController('Game').updatePiecesInHand(game);
                }
                return;
            }
        }

        m = this.gameEndRe.exec(line);
        if (m) {
            gameID = parseInt(m[1], 10);
            if (gameID in mater.model.Game.byID) {
                game = mater.model.Game.byID[gameID];
                game.gameEnd(m[2], m[3]);
                this.application.fireEvent('gameend', game, m[2], m[3]);
            }
            else {
                // XXX
                window.alert('could not find game ' + gameID);
            }
            this.consoleLines.push(line);
            return;
        }

        m = this.style12Re.exec(line);
        if (m) {
            var data = {
                board: m[1],
                turn: m[2],
                ep: parseInt(m[3], 10),
                wOO: parseInt(m[4], 10),
                wOOO: parseInt(m[5], 10),
                bOO: parseInt(m[6], 10),
                bOOO: parseInt(m[7], 10),
                fiftyCount: parseInt(m[8], 10),
                gameID: parseInt(m[9], 10),
                white: m[10],
                black: m[11],
                relation: parseInt(m[12], 10),
                //initialTime: m[13],
                //inc: m[14],
                //wMat: m[15],
                //bMat: m[16],
                whiteTime: parseInt(m[17], 10),
                blackTime: parseInt(m[18], 10),
                fullMoveNum: parseInt(m[19], 10),
                //verboseMove: m[20],
                elapsed: parseInt(m[21], 10),
                san: m[22],
                flip: parseInt(m[23], 10),
                clockIsTicking: parseInt(m[24], 10),
                lag: parseInt(m[25], 10)
            };
            data.ply = 2 * (data.fullMoveNum - 1) + (data.turn === 'W' ? 0 : 1);

            // If gameID is not found, then create a game.
            if (!(data.gameID in mater.model.Game.byID) ||
                !mater.model.Game.byID[data.gameID].active) {
                if (!this.gameData) {
                    console.log('gameData missing for game ' + data.gameID);
                    console.log(data);
                    return;
                }
                if (this.gameData.hasOwnProperty('white')) {
                    console.assert(this.gameData.white === data.white);
                    console.assert(this.gameData.black === data.black);
                }
                for (var attr in this.gameData) {
                    if (this.gameData.hasOwnProperty(attr)) {
                        data[attr] = this.gameData[attr];
                    }
                }
                if (data.gameID in this.pendingGameMessages) {
                    data.initialChat = this.pendingGameMessages[data.gameID];
                    delete this.pendingGameMessages[data.gameID];
                }
                data.game = Ext.create('mater.model.Game', data);

                console.assert(data.gameID in mater.model.Game.byID);
                this.gameData = null;
            }

            else {
                console.assert(this.gameData === null);
                data.game = mater.model.Game.byID[data.gameID];
            }

            // style12 board to FEN
            data.fen = data.board.substr(0, data.board.length - 1);
            data.fen = data.fen.replace(this.spaceRe, '/');
            data.fen = data.fen.replace(this.style12DashRe, function(s, m1) {
                return m1.length;
            });
            data.fen = data.fen + ' ' + data.turn.toLowerCase() + ' ';
            if (data.wOO) {
                data.fen += 'K';
            }
            if (data.wOOO) {
                data.fen += 'Q';
            }
            if (data.bOO) {
                data.fen += 'k';
            }
            if (data.bOOO) {
                data.fen += 'q';
            }
            if (!(data.wOO || data.wOOO || data.bOO | data.bOOO)) {
                data.fen += '-';
            }
            data.fen += ' ';
            if (data.ep === -1) {
                data.fen += '-' + ' ';
            }
            else {
                data.fen += String.fromCharCode('a'.charCodeAt(0) + data.ep);
                if (data.turn === 'W') {
                    data.fen += '6 ';
                }
                else {
                    data.fen += '3 ';
                }
            }
            data.fen += data.fiftyCount + ' ' + data.fullMoveNum;

            // The standard way to communicate between controllers
            // is to fire an event, but when using events there
            // seemed to be issues with the Game controller
            // seeing them out of order.
            this.application.getController('Game').update(data);
            return;
        }

        m = this.compressmoveRe.exec(line);
        if (m) {
            var data2 = {
                gameID: parseInt(m[1], 10),
                ply: parseInt(m[2], 10),
                san: m[3],
                // smith: m[4],
                fen: null,
                elapsed: parseInt(m[5], 10),
                remaining: parseInt(m[6], 10),
                lag: parseInt(m[7], 10)
            };
            if (!(data2.gameID in mater.model.Game.byID) || !mater.model.Game.byID[data2.gameID].active) {
                console.log('got unexpected <d1> line for game ' + data2.gameID);
            }
            data2.game = mater.model.Game.byID[data2.gameID];
            this.application.getController('Game').update(data2);
            return;
        }

        m = this.examineRe.exec(line);
        if (m) {
            // we don't get rating info for examined games
            console.assert(this.gameData === null);
            this.gameData = {
                whiteRating: null,
                blackRating: null,
                whiteHasTimeseal: false,
                blackHasTimeseal: false,
                desc: 'examined game'
            };
            this.consoleLines.push(line);
            return;
        }

        m = this.bsetupRe.exec(line);
        if (m) {
            // TODO
            console.assert(this.gameData === null);
            this.gameData = {
                whiteRating: null,
                blackRating: null,
                whiteHasTimeseal: false,
                blackHasTimeseal: false,
                desc: 'examined game (setup mode)'
            };
            this.consoleLines.push(line);
        }

        m = this.gameMsgRe.exec(line);
        if (m) {
            gameID = parseInt(m[1], 10);
            if (!(gameID in mater.model.Game.byID) || !mater.model.Game.byID[gameID].active)
            {
                this.consoleLines.push(line);
                this.pendingGameMessages[gameID] = m[2];
                return;
            }
            else {
                game = mater.model.Game.byID[gameID];
                game.addChat(m[2]);
                return;
            }
        }

        m = this.commentRe.exec(line);
        if (m) {
            gameID = parseInt(m[3], 10);
            if (!(gameID in mater.model.Game.byID) || !mater.model.Game.byID[gameID].active)
            {
                this.consoleLines.push(line);
                console.log('got unexpected says for game ' + gameID);
                return;
            }
            else {
                game = mater.model.Game.byID[gameID];
                game.addChat(m[1] + m[2] + m[4]);
                return;
            }
        }

        m = this.unexRe.exec(line);
        if (m) {
            gameID = m[1];
            this.consoleLines.push(line);
            mater.model.Game.byID[gameID].close();
            return;
        }

        m = this.unobRe.exec(line);
        if (m) {
            this.consoleLines.push(line);
            gameID = parseInt(m[1], 10);
            mater.model.Game.byID[gameID].stop();
            return;
        }

        m = this.announceRe.exec(line);
        if (m) {
            this.application.fireEvent('news', line);
            return;
        }
    },

    loadSettings: function(model) {
        "use strict";
        if (!model) {
            model = Ext.create('mater.model.Settings');
            model.setId(mater.username);
        }
        mater.settings = model;
        this.loginState = 'LoggedIn';
        this.application.fireEvent('icsloggedin', mater.username);
        this.sendIcsRc();
    },

    sendIcsRc: function() {
        "use strict";
        var script = mater.settings.data.logonScript;
        this.sendLine("set interface mater v" + mater.VERSION);
        this.sendLine("set style 12");
        this.sendLine("set bell 0");
        if (!this.amGuest) {
            this.sendLine("mess u\n");
        }
        var lines = script.split('\n');
        if (lines.length > 0 && lines[lines.length - 1] === '') {
            lines.pop(); // chomp
        }
        for (var i = 0; i < lines.length; i++ ) {
            var line = lines[i].replace(/%user%/g, mater.username).replace(/%version%/g, mater.VERSION);
            this.sendLine(line);
        }
    },

    // lag timeout
    lagTimer: null,
    resetLagTimer: function() {
        "use strict";
        if (this.lagTimer) {
            window.clearTimeout(this.lagTimer);
        }
        this.lagTimer = window.setTimeout(this.timeoutWarning, 30000);
    },

    timeoutWarning: function() {
        "use strict";
        Ext.MessageBox.show({
            title: 'Connection warning',
            msg: 'The connection with the server is not responding. This problem may be caused network lag at your local connection or lag between your computer and the server. It is also possible the connection was lost entirely.',
            width: 300,
            icon: Ext.MessageBox.WARNING,
            buttons: Ext.MessageBox.OK
        });
    }
});

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent

