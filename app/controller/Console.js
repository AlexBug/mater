/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

/* global console */

Ext.define('mater.controller.Console', {
    extend: 'Ext.app.Controller',

    views: ['Console'],

    refs: [
        {
            ref: 'consolewin',
            selector: 'mainconsole'
        },
        {
            ref: 'commandtext',
            selector: 'textfield[id=commandText]'
        },
        {
            ref: 'consoletext',
            selector: 'container[id=consoleText]'
        },
        {
            ref: 'showconsolecheckbox',
            selector: 'mainmenu #showConsole'
        }
    ],

    hidden: true,

    init: function() {
        "use strict";
        var el;
        this.control({
            textfield: {
                specialkey: function(ct, e) {
                    if (this.getConsolewin().isHidden()) {
                        return;
                    }
                    if (ct.id !== 'commandText') {
                        return;
                    }
                    switch (e.getKey()) {
                        case e.ENTER:
                            this.doCommand(ct.getValue());
                            ct.setValue('');
                            break;
                        case e.UP:
                            this.historyUp(ct);
                            break;
                        case e.DOWN:
                            this.historyDown(ct);
                            break;
                        case e.PAGE_UP:
                            el = this.getConsoletext().getEl();
                            el.setScrollTop(el.getScrollTop() - 250);
                            break;
                        case e.PAGE_DOWN:
                            el = this.getConsoletext().getEl();
                            el.setScrollTop(el.getScrollTop() + 250);
                            break;
                        default:
                            return false;
                    }
                    return true;
                }
            }
        });
        this.control({
            mainconsole: {
                show: function() {
                    this.getShowconsolecheckbox().setChecked(true);
                    this.getConsoletext().el.setScrollTop(999999);
                    Ext.defer(function() {
                        this.getConsolewin().focus();
                        this.getCommandtext().focus();
                    }, 200, this);
                },
                minimize: function() {
                    this.hide();
                },
                hide: function() {
                    this.getShowconsolecheckbox().setChecked(false);
                }
            },
            // XXX
            // this element seems to get afterrender events but never
            // focus events.
            // How to force focus back to the textbox?
            /*'container[id=consoleText]': {
                focus: function(e) {
                    this.getCommandtext().focus();
                }
            }*/
        });
        this.application.on({
            gotlines: this.gotLines,
            icsloggedin: function(username) {
                var c = this.getConsolewin();
                c.setTitle('Console - ' + username);
                // re-enable the command input in case it was
                // previously disabled
                c = this.getCommandtext();
                c.enable();

                // Alex Guo: show console if checkbox was checked.
                if (this.getShowconsolecheckbox().checked) {
                    console.log("showing console right now...");
                    this.getConsolewin().show();
                }

                // this doesn't really belong here; maybe
                // there should be a separate controller for it
                Ext.getDoc().on('keydown', this.keyDown, this);
            },
            icslostconnection: this.lostConnection,
            scope: this
        });
    },

    keyDown: function(e) {
        "use strict";
        switch (e.keyCode) {
            case Ext.EventObject.F2:
                var win = this.getConsolewin();
                win.setVisible(!win.isVisible());
                break;
            /*case Ext.EventObject.F11:
                mater.app.getMainMenuController().showSettings();
                e.preventDefault();
                break;*/
        }
    },

    statics: {
        linkRe: /(\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim,
        bcsRe: /\!BCS-&gt;\((\w+)\)/gm,
        commandRe: /&quot;((?:alias|unalias|quit|tell|say|xtell|td|mam|set|finger|shout|cshout|observe|match|messages|clearmessages|seek|unseek|refresh|adjourn|inchannel|variables|moretime|takeback|whisper|xwhisper|kibitz|xkibitz|time|who|logons|llogons|news|addlist|sublist|showlist|help|forward|backward|best|hbest|pstat|oldpstat|ping|examine|unexamine|follow|date|flag|mexamine|statistics|it|stored|accept|decline|journal|jsave|jkill|limits|ginfo|ivariables|showadmins|showsrs|showtms)(?: [^"'&<>]+?)?)&quot;/gm,
        linkify: function(txt) {
            // called after the string has been HTML encoded
            "use strict";
            txt = txt.replace(mater.controller.Console.linkRe, function(s, url) {
                return '<a href="' + Ext.String.htmlDecode(url) +
                    '">' + url + '</a>';
            });
            txt = txt.replace(mater.controller.Console.commandRe, function(s, cmd) {
                console.assert(cmd.indexOf("'") === -1);
                return '&quot;<a href="#" title="Click to execute command" onclick="mater.app.getController(\'Console\').doCommand(\'' + cmd + '\'); return false;">' + cmd + '</a>&quot;';
            });
            txt = txt.replace(mater.controller.Console.bcsRe, function(s, txt) {
                var ret = '<img style="width: 16px; height: 16px; border: 0" alt="Sound" src="resources/img/sound.png" />(' + txt + ')';
                var sound;
                switch(txt) {
                    case "bell":
                        sound = txt;
                        break;
                    default:
                        sound = null;
                        break;
                }
                if (sound) {
                    ret = '<a href="#" onclick="mater.app.getController(\'Sound\').play(\'' + sound + '\'); return false;">' + ret + '</a>';
                }
                return ret;
            });
            return txt;
        }
    },

    gotLines: function(lines) {
        "use strict";
        // Ugly hack to focus on the input box when clicking on the console.
        // I couldn't find a way to capture the click event in a parent
        // element.
        var getctid = 'Ext.get(' + this.getConsoletext().getId() + ')';
        var hline = '<div onclick="' + getctid + '.focus()"">';
        for (var i = 0; i < lines.length; i++) {
            hline = hline + Ext.String.htmlEncode(lines[i]) + '<br />';
        }
        hline = mater.controller.Console.linkify(hline + '</div>');
        this.add(hline);
    },

    add: function(html) {
        "use strict";
        var ct = this.getConsoletext();
        ct.add({html: html});

        // scroll to bottom
        if (ct.el) {
            ct.el.setScrollTop(999999);
        }
    },

    cmdHist: [],
    cmdHistIndex: 0,
    cmdHistMax: 10,
    doCommand: function(cmd) {
        "use strict";
        if (cmd.length >= 1024) {
        }
        else {
            this.application.fireEvent('sendline', cmd);
            this.add('<div class="sentCommand">&gt; ' + cmd + '</div>');
            this.cmdHist.push(cmd);
            if (this.cmdHist.length > this.cmdHistMax) {
                this.cmdHist.shift();
            }
            this.cmdHistIndex = this.cmdHist.length;
        }
    },

    historyUp: function(ct) {
        "use strict";
        if (this.cmdHistIndex > 0) {
            this.cmdHistIndex--;
            ct.setValue(this.cmdHist[this.cmdHistIndex]);
        }
    },

    historyDown: function(ct) {
        "use strict";
        if (this.cmdHistIndex < this.cmdHist.length) {
            this.cmdHistIndex++;
            if (this.cmdHistIndex === this.cmdHist.length) {
                ct.setValue('');
            }
            else {
                ct.setValue(this.cmdHist[this.cmdHistIndex]);
            }
        }
    },

    lostConnection: function() {
        "use strict";
        var c = this.getConsolewin();
        if (c && c.title && c.title.indexOf('[inactive]') === -1) {
            c.setTitle(c.title + ' [inactive]');
        }
        // we don't really want to remove this listener until we try
        // to log in again
        //Ext.getDoc().un('keydown', this.keyDown, this);

        c = this.getCommandtext();
        c.disable();
    }

});

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
