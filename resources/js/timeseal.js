/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

/* global console:false, window:false, navigator:false */
/* exported Timeseal */

var Timeseal = {
    // FICS timeseal 2 (note that so-called timeseal 2 uses the same
    // encryption string that refers to v1.0)
    timesealKey: "Timestamp (FICS) v1.0 - programmed by Henrik Gram.",

    _bswap: function(s, i, j) {
        "use strict";
        console.assert(i < j);
        var t = s.substr(0, i) + s.charAt(j) + s.substr(i + 1, j - i - 1) +
            s.charAt(i) + s.substr(j + 1);
        //alert('modified ' + i + ',' + j + ': ' + s);
        return t;
    },

    timesealEncode: function(line) {
        "use strict";

        var d = new Date();
        var ts = d.getTime() % 10000000;
        line = line + '\x18' + ts.toString(10) + '\x19';
        var len = line.length;
        while (len % 12 !== 0) {
            len++;
            line += '1';
        }
        for (var n = 0; n < len; n += 12) {
            line = this._bswap(line, n + 0, n + 11);
            line = this._bswap(line, n + 2, n + 9);
            line = this._bswap(line, n + 4, n + 7);
        }
        console.assert(len === line.length);
        var newline = '';
        for (n = 0; n < len; n++) {
            var c = ((line.charCodeAt(n) | 0x80) ^ this.timesealKey.charCodeAt(n % 50)) - 32;
            /*if (c < 0) {
                c = c + 0xff + 1
            }*/
            console.assert(c > 0);
            console.assert(c < 256);
            newline += String.fromCharCode(c);
        }
        newline = newline + '\x80\x0a';
        len += 2;
        // SockJS can't handle binary data, so base64-encode it
        newline = window.btoa(newline);
        return newline;
    },

    // same for timeseal and zipseal
    pingG: '[G]\0',
    pongReply: '\x02' + '9',

    tsHandlePings: function(line, conn) {
        "use strict";
        while (true) {
            var i = line.indexOf(this.pingG);
            if (i === -1) {
                break;
            }
            conn.send(this.timesealEncode(this.pongReply));
            line = line.substring(0, i) + line.substring(i + 4);
        }
        return line;
    },

    timesealDecode: function(line, conn) {
        "use strict";
        line = this.tsHandlePings(line, conn);
        return line;
    },


    timesealHello: function() {
        "use strict";
        // Alex Guo: BICS only supports TIMESTAMP, not TIMESEAL2.
        return 'TIMESTAMP|materUser|'+navigator.userAgent+'|';
        //return 'TIMESEAL2|materUser|'+navigator.userAgent+'|';
    },


    // FatICS zipseal

    // add a timestamp to the given line, for sending to the server
    zipsealEncode: function(line) {
        "use strict";
        var d = new Date();
        var ts = d.getTime() % 10000000;
        // Unlike the FICS timeseal equivalent, the input and output to this
        // function should be valid UTF-8, so we don't need to base64-encode it
        return line + '\x18' + ts.toString(16) + '\n';
    },


    // send the reply string with timestamp whenever we get [G] from the server
    zsHandlePings: function(data, conn) {
        "use strict";
        while (true) {
            var i = data.indexOf(this.pingG);
            if (i === -1) {
                break;
            }
            conn.c.resetLagTimer();
            conn.send(this.zipsealEncode(this.pongReply));
            data = data.substring(0, i) + data.substring(i + 4);
        }
        return data;
    },

    zipsealDecode: function(line, conn) {
        "use strict";
        line = this.zsHandlePings(line, conn);
        return line;
    },

    zipsealHello: function() {
        "use strict";
        var major = 1;
        var minor = 0;
        return 'ZIPSEAL|'+major+'|'+minor+'|materUser|'+navigator.userAgent+'|';
    }
};

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
