/*
 * Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
 *
 * This file is part of Mater.
 *
 * Mater is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mater is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mater.  If not, see <http://www.gnu.org/licenses/>.
 */

/* global document */

Ext.Loader.setConfig({
    disableCaching: false
});

Ext.application({
    requires: ['Ext.container.Viewport'],
    name: 'mater',

    appFolder: 'app',
    controllers: [
        'MainMenu', 'Login', 'Ics', 'Game', 'Console', 'Sound', 'Chat',
        'Channels', 'News', 'Finger'
    ],

    init: function() {
        "use strict";
        mater.VERSION = '0.3';

        // get server type
        if (document.location.search.indexOf('ics=fics') !== -1) {
            mater.serverType = 'fics';
            mater.icsName = 'BICS';
        }
        else {
            mater.serverType = 'fatics';
            mater.icsName = 'Bughouse ICS';
        }
    },

    launch: function() {
        "use strict";

        //Ext.FocusManager.enable({focusFrame: true});
        Ext.create('Ext.container.Viewport', {
            //layout: 'fit',
            items: [
                {
                    xtype: 'panel',
                    dockedItems: {
                        xtype: 'mainmenu'
                    },
                },
                {
                    xtype: 'loginwindow'
                },
                {
                    xtype: 'mainconsole'
                },
                {
                    xtype: 'chat'
                },
                {
                    xtype: 'channels'
                },
                {
                    xtype: 'settings'
                },
                { xtype: 'news' }
                /*{
                    // "Any Container using the Border layout must
                    // have a child item with region:'center'."
                    xtype: 'container',
                    region: 'center'
                }*/
            ]
        });
    }
});

Ext.getDoc().on('keydown', function(e) {
    "use strict";
    switch (e.keyCode) {
        case Ext.EventObject.ESC:
            // prevent the ESC key from stopping the application
            e.preventDefault();
            break;
    }
    return true;
});

// workaround for http://www.sencha.com/forum/showthread.php?265347-4.2.1-Dragging-window-bad-Z-index&p=972728
Ext.define('mater.Overrides', {}, function () {
    /* jshint strict: false */
    Ext.require([
        'Ext.window.Window'
    ],
    function () {
        Ext.window.Window.override({
            initDraggable: function () {
                this.callParent();
                this.dd.on('drag', function () {
                    this.ghostPanel.setZIndex(Ext.WindowManager.getActive().getEl().dom.style.zIndex);
                }, this);
            }
        });
    });
});

// vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent
