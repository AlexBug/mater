# Copyright (C) 2014  Wil Mahan <wmahan@gmail.com>
#
# This file is part of Mater.
#
# Mater is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Mater is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Mater.  If not, see <http://www.gnu.org/licenses/>.

"""
a simple SockJS proxy to FICS using txsockjs
"""

import base64
import sys

from twisted.application import service, internet
#from twisted.internet import ssl
from twisted.python import log
from twisted.protocols import portforward
from twisted.tap.portforward import Options

# import the epoll reactor here instead of using the -r
# option to twistd to avoid the problem in twisted bug #3785
from twisted.internet import epollreactor
epollreactor.install()
from twisted.internet import reactor

from txsockjs.factory import SockJSFactory

if len(sys.argv) < 4:
    print "Need 3 args."
    print "Usage: <host> <port> <proxyport>"

HOST = sys.argv[1]
PORT = int(sys.argv[2])
WEBSOCKET_PORT = int(sys.argv[2])
#HOST = 'alexbug.com'
#WEBSOCKET_PORT = 5001
#PORT = 5000
timeseal = True

class LoggingProxyClient(portforward.ProxyClient):
    def dataReceived(self, data):
        log.msg(data)
        if type(data) == unicode:
            data = data.encode('utf-8')
        else:
            # The only time the server should gnore bytes that are not
            # valid UTF-8 is at the password prompt, when it sends
            # telnet control characters to hide the password. Since
            # Mater uses a dialog box for the password it doesn't
            # need those control characters, it should be safe to
            # strip them out so that txsockjs doesn't get confused.
            #data = data.decode('utf-8').encode('utf-8')
            data = data.decode('utf-8', 'ignore').encode('utf-8', 'ignore')
        #log.msg('server sent: ' + repr(data))

        portforward.ProxyClient.dataReceived(self, data)

class LoggingProxyClientFactory(portforward.ProxyClientFactory):
    protocol = LoggingProxyClient

class LoggingProxyServer(portforward.ProxyServer):
    clientProtocolFactory = LoggingProxyClientFactory
    sentIP = False

    def dataReceived(self, data):
        if type(data) == unicode:
            data = data.encode('utf-8')
        if timeseal:
            data = base64.b64decode(data)
        #log.msg('client sent: ' + repr(data))
        #if not self.sentIP:
        #    self.sentIP = True

        #    # insert a line to set the remote IP
        #    data = ('%%i%s\n' % self.transport.getPeer().host) + data

        portforward.ProxyServer.dataReceived(self, data)

class LoggingProxyFactory(portforward.ProxyFactory):
    protocol = LoggingProxyServer

def start_services():
    #cf = ssl.DefaultOpenSSLContextFactory('keys/server.pem', 'keys/server.key')
    #service = internet.SSLServer(WEBSOCKET_PORT,
    #    SockJSFactory(LoggingProxyFactory(HOST, PORT)), cf)
    service = internet.TCPServer(WEBSOCKET_PORT, SockJSFactory(LoggingProxyFactory(HOST, PORT)))
    service.setServiceParent(application)

application = service.Application("ficsproxy")

start_services()


# vim: expandtab tabstop=4 softtabstop=4 shiftwidth=4 smarttab autoindent ft=python
